<?php 
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API  

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues] 

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
    
/**
 * Multipurpose logger / tracer 
 *
 */ 

 namespace Developer;


class Log {

  /* -------------- VAR DECLARIONS  ------------------------------------------*/
	protected static $ENABLED = false;
	protected static $DEBUG_FILE = NULL;
	protected static $ERROR_FILE = NULL;
	protected static $EXCEPTION_FILE = NULL;
	protected static $SHUTDOWN_ERROR_FILE = NULL;
  protected static $SNAPSHOTS = array();
  protected static $start_ts = 0;

 /* 
 * \Developer::getDev()
 * -------------------------------
 * Based on an environment var set in the apache conf
 * we determine if this is a dev environment
 * e.g <vhost> SetEnv DGE_DEVELOP_ENV jd ...
 * returns FALSE when this is a PRODUCTION ENVIRONMENT
 * 
 *
 * @return string developer initials || bool false
 */
  public static function getDev(){
    return getenv('DGE_DEVELOP_ENV');
  }

	/*
	*	Write data to a log/debug file
	* The file can be tail(ed) for easy tracing and debugging
	*
	* @param $o , any type of data
	* @param $fp, File Pointer, optional alternative File to write to
	*/
	public static function debug($o, $fp = false){

		if(! self::$ENABLED ){
			return;
		}

		$o = self::_presentObject($o);
		$o = var_export($o, true). PHP_EOL;

		if(!$fp){
			$fp = self::$DEBUG_FILE;
		}

		if( // We need a file to write to
			$fp
			&& is_string($fp)
		){

			if(!is_file($fp)){
        if(false === file_put_contents($fp, '', FILE_APPEND)) {
          throw new \Exception('Invalid debug log file', 1); 
        } 
				
			}else if(!is_writable($fp)){
				throw new \Exception("Invalid debug log file", 1);  
			}

			$info = 'DEBUG:'.date("Y-m-d.H.i.s"). PHP_EOL;
			file_put_contents($fp, $info, FILE_APPEND);
			file_put_contents($fp, $o, FILE_APPEND);
		}else{
			// Jammer Joh :(
			throw new \Exception("Unable to write debug data", 1);  
		}

	}

  /*
   *  Take a memory SNAPSHOT
   *
   * @param $key, string, readable index instead of a number  
   */
  public static function takeSnapShot($desc = null){

    if($desc && is_string($desc)){
      // Nothing
    }else{
      $desc = '';
    }
    $t = self::getTime();
    
    if(count(self::$SNAPSHOTS) < 1){
      self::$start_ts = $t;
    }

    $t  = $t - self::$start_ts;

    $sh = array(
      'comment' => $desc,
      'memory' => self::convertByte(memory_get_usage(true)),
      'peak_memory' => self::convertByte(memory_get_peak_usage(true)),
      'seconds' => $t
    );
    array_push ( self::$SNAPSHOTS , $sh);

  }

  public static function getSnapShots(){
    return self::$SNAPSHOTS;
  }

	/* set a standard debug file to write to */
	public static function setDebugFile($fp){
		if( // We need a file to write to
			$fp
			&& is_string($fp)
		){
			if(!is_file($fp)){
				file_put_contents($fp, '', FILE_APPEND);
			}else if(!is_writable($fp)){
				throw new \Exception("Invalid debug file", 1);  
			}
			self::$DEBUG_FILE = $fp;

		}else{
			throw new \Exception("Invalid debug file", 1);  
		}
	}
	

	public static function enableDebugging(){
		self::$ENABLED = true;
	}

	public static function disableDebugging(){
		self::$ENABLED = false;
	}

	/*
  * Developer\Log::handleErrors()
  * -------------------------------
  *
  * Collect errors and write them to a log file 
  *
  * @param $fp string to WRITEABLE FILE
  * 
  */
  public static function handleErrors($fp){

  	if( // We need a file to write to
		$fp
		&& is_string($fp)
	){

    	if(!is_file($fp)){
        if(false === file_put_contents($fp, '', FILE_APPEND)) {
          throw new \Exception('Invalid error log file', 1); 
        }
    	}else if(!is_writable($fp)){
    		throw new \Exception("Invalid error log file", 1);  
    	}

		  self::$ERROR_FILE = $fp;

		}else{
			throw new \Exception("Invalid error log file", 1);  
		}

		set_error_handler( function($error, $message = '', $file = '', $line = 0 ){
			$s = $error . ': ' . $message . " in " . $file . ' line ' . $line.PHP_EOL;
			\Developer\Log::writeError($s);
		});

		self::handleExceptions();
		self::handleShutDownErrors();        
  }

  public static function writeError($s){
  	if( 
  		// Need a string and an error file to write to 
  		   !is_string($s)
  		|| !self::$ERROR_FILE

  	){
  		return;
  	}
  	$info = 'ERROR:'.date("Y-m-d.H.i.s");
  	file_put_contents(self::$ERROR_FILE, $info.PHP_EOL, FILE_APPEND);
  	file_put_contents(self::$ERROR_FILE, $s.PHP_EOL, FILE_APPEND);
    // file_put_contents(self::$ERROR_FILE, self::getBacktrace().PHP_EOL, FILE_APPEND);
  }

  /*
  * Developer\Log::writeException()
  * -------------------------------
  * Append a string to the EXCEPTION file pointer
  *
  */
  public static function writeException($s){
  	if( 
  		// Need a string and an error file to write to 
  		   !is_string($s)
  		|| !self::$EXCEPTION_FILE

  	){
  		return;
  	}
  	$info = 'EXCEPTION:'.date("Y-m-d.H.i.s");
  	file_put_contents(self::$EXCEPTION_FILE, $info.PHP_EOL, FILE_APPEND);
  	file_put_contents(self::$EXCEPTION_FILE, $s.PHP_EOL, FILE_APPEND);
    file_put_contents(self::$EXCEPTION_FILE, self::getBacktrace().PHP_EOL, FILE_APPEND);
  }

  /*
   * self::getBacktrace()
   * --------------------
   * Get a backtrace as string
   *
   *
   */
  public static function getBacktrace($ignore = 2) { 
    $trace = ''; 

    try {
      foreach (debug_backtrace() as $k => $v) { 
        if ($k < $ignore) { 
          continue; 
        } 
        array_walk($v['args'], function (&$item, $key) { 
          $item = var_export($item, true); 
        }); 

        $trace .= '#' . ($k - $ignore) . ' ' . $v['file'] . '(' . $v['line']  . '): ' . (isset($v['class']) ? $v['class'] . '->' : '') . $v['function'] . '(' . implode(', ', $v['args']) . ')' . "\n"; 
    } 
      
    } catch (Exception $e) {
      
    }
    return $trace; 
  } 


  /*
  * Developer\Log::handleExceptions()
  * -------------------------------
  *
  * Collect exceptions and write them to a log '/data/error.log'
  */
  private static function handleExceptions($fp = false){

  	if(!$fp && self::$ERROR_FILE){
  		// We use the error file for exceptions
  		self::$EXCEPTION_FILE = self::$ERROR_FILE;
  	}else{
  		if( // We need a file to write to
		$fp
		&& is_string($fp)
		){
			if(!is_file($fp)){
				file_put_contents($fp, '', FILE_APPEND);
			}else if(!is_writable($fp)){
				throw new \Exception("Invalid exception log file", 1);  
			}
			self::$EXCEPTION_FILE = $fp;

			}else{
				throw new \Exception("Invalid exception log file", 1);  
			}
  	}
  	set_exception_handler(function($e){
      // @TODO: add debug_backtrace(?) 
  		$s = get_class($e)." thrown within the exception handler. Message: ".$e->getMessage()." on line ".$e->getLine();
  		\Developer\Log::writeException($s);
  	});
  }

  /*
  * Developer\Log::handleShutDownErrors()
  * ----------------------------------------
  *
  * Fatal errors needs to be logged as well
  * Really meaning full
  *
  */
  private static function handleShutDownErrors(){
    if(!self::$ERROR_FILE){
    	return;
    }
    register_shutdown_function(function (){
      if ($e = error_get_last()) {
        $s = $e['message'] . " in " . $e['file'] . ' line ' . $e['line'];
        \Developer\Log::writeError($s);
      }
    });
      
  }

  /* ------------- PRIVATES --------------------------------------------------*/

 /*
  * self::_presentObject()
  * -------------------------------
  *
  * Some objects do not have the magic __toString() method
  * With an unlimited number of circular references
  * we need a simple presentation of complicated objects
  * 
  * returns an object with __toString() magic
  */
  private static function _presentObject($var){
    if (is_object($var) && ! method_exists($var, '__toString')) {
      $classn = get_class($var);
      $classm = get_class_methods($var);
      $classp = get_parent_class($var);
      $classv = get_class_vars ($classn);
      $classprops = get_object_vars ($var);
      $ret_val = array(
        '_class_name'=>$classn, '_parent_class'=>$classp,
        '_class_methods'=>$classm,'_class_vars'=>$classv,
        '_objects_props'=>$classprops
      ); 
      return $ret_val;
    }else{
        return $var;
    }
  }

   private static function getTime() {
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    return $time;
  }
  
  private static function getReadableTime($time) {
    $ret = $time;
    $formatter = 0;
    $formats = array('ms', 's', 'm');
    if($time >= 1000 && $time < 60000) {
            $formatter = 1;
            $ret = ($time / 1000);
    }
    if($time >= 60000) {
            $formatter = 2;
            $ret = ($time / 1000) / 60;
    }
    $ret = number_format($ret,3,'.','') . ' ' . $formats[$formatter];
    return $ret;
  }

  /* convert bytes sizes to something meaningful */
  private static function convertByte($size){
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
  }
  
} 
