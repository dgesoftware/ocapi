<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API  

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues] 

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
    
/**
* Mysqli wrapper helper class
*
*/

namespace Dge;

class DBException extends \Exception {
}

namespace Mysql;

class Db {
  private $db = false;
  private $host;
  private $port;
  private $user;
  private $password;
  private $database;
  private $socket;

  private $profiling;
  /* FOR LOGGING PURPOSES */
  public $queryCount = 0;
  public $queryDoubleCount = 0;
  public $queries = array();

  public function __construct($host, $user, $password, $database, $port, $socket) {
    $this->host = $host;
    $this->port = $port;
    $this->user = $user;
    $this->password = $password;
    $this->database = $database;
    $this->socket = $socket;
    $this->profiling = false;
  }

  public function __destruct() {
    if ($this->db && is_object($this->db)) {
      mysqli_close($this->db);
    }
  }

  # Get the active database name
  #
  # * returns string database name or false
  public function getActiveDatabaseName(){
    return $this->database;
  }

  public function enableProfiling(){
    $this->profiling = true;
    return $this;
  }

  public function disableProfiling(){
    $this->profiling = false;
    return $this;
  }

  public function check() {
    if (! (isset($this->db) && is_object($this->db))) {
      return $this->connect();
    }
    return $this;
  }

  public function data_seek($res, $index) {
    mysqli_data_seek($res, $index);
  }

  public function get_handle() {
    $this->check();
    return $this->db;
  }

  public function addslashes($string) {
    return $this->escape_string($string);
  }

  public function escape_string($string) {
    $this->check();
    if (get_magic_quotes_gpc()) {
      $string = stripslashes($string);
    }
    return mysqli_real_escape_string($this->db, $string);
  }

  public function connect() {
    if ($this->db && is_object($this->db)) {
      $this->close();
    }
    if (! $this->db = mysqli_connect($this->host, $this->user, $this->password, $this->database, $this->port, $this->socket)) {
      throw new \Dge\DBException("Could not connect to DB Host: " . mysqli_connect_error());
    }else{
      $this->db->set_charset("utf8");
      /*
      $this->query("SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE");
      $this->query("SET SESSION sql_mode = 'ANSI'");
      $this->query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");
      $this->query("SET CHARACTER SET 'utf8'");
       */    
    }
    return $this;
  }

  public function close() {
    if ($this->db) {
      mysqli_close($this->db);
      $this->db = false;
    }
    return $this;
  }

  public function change_user($user, $password) {
    $this->check();
    $this->user = $user;
    $this->password = $password;
    if (! @mysqli_change_user($user, $password)) {
      throw new \Dge\DBException("Error in DB Change User user: $user, MySql Error: " . mysqli_error($this->db));
      return false;
    }
    return true;
  }

  public function select_db($db) {
    $this->check();
    if (! @mysqli_select_db($this->db, $db)) {
      throw new \Dge\DBException("Could not Select DB: $db. MySql Error: " . $this->error());
    }
    $this->database = $db;
    return $this;
  }

  public function error() {
    return @mysqli_error($this->db);
  }

  public function prepare($statement) {
    $this->check();
    if (! $ret = @mysqli_prepare($this->db, $statement)) {
      throw new  \Dge\DBException("Error in Prepare statement on db {$this->database}, MySql Error: " . mysqli_error($this->db) . "<br>Statement: $statement<br>");
    }
    return $ret;
  }

  public function execute($res) {
    if (! $ret = @mysqli_stmt_execute($res)) {
      throw new  \Dge\DBException("Error in Execute statement on db {$this->database}, MySql Error: " . mysqli_error($this->db));
    }
    return $ret;
  }

  public function query($query) {
    $this->check();
    if($this->profiling){
      $start = $this->getTime();
    }
        
    if (! $ret = @mysqli_query($this->db, $query)) {
      throw new  \Dge\DBException("Error in Query on db {$this->database}, MySql Error: " . mysqli_error($this->db) . "<br>Query: $query<br>");
    }
    if($this->profiling){
      $this->logQuery($query, $start);
    }
    return $ret;
  }

  public function fetch_row($res) {
    return @mysqli_fetch_row($res);
  }

  public function fetch_array($res, $result_type = MYSQLI_ASSOC) {
    return @mysqli_fetch_array($res, $result_type);
  }

  public function num_rows($res) {
    return @mysqli_num_rows($res);
  }

  public function num_fields($res) {
    return @mysqli_num_fields($res);
  }

  public function affected_rows() {
    return @mysqli_affected_rows($this->db);
  }

  # @TODO: this is not working as expected
  public function insert_id() {
   //  if (! $ret = $this->db->insert_id) {
   // // if (! $ret = mysqli_insert_id($this->db)) {
   //   // \Developer\Log::debug($ret);
   //    // throw new \Mysql\DBException("Error in insert_id, MySql Error: " . $this->error());
   //  }
    $ret = $this->db->insert_id;
    return $ret;
  }

  public function field_len($res, $field) {
    return @mysqli_field_len($res, $field);
  }

  public function field_name($res, $field) {
    return @mysqli_field_name($res, $field);
  }

  public function field_type($res, $field) {
    return @mysqli_field_type($res, $field);
  }

  public function list_fields($db, $table) {
    $this->check();
    return @mysqli_list_fields($db, $table, $this->db);
  }

  public function count_fields() {
    $this->check();
    return @mysqli_field_count($this->db);
  }

  public function list_tables($db) {
    $this->check();
    return @mysqli_list_tables($db, $this->db);
  }

  public function list_dbs() {
    $this->check();
    return @mysqli_list_dbs($this->db);
  }

  public function name($res) {
    return @mysqli_name($res);
  }

  public function tablename($res, $row) {
    return @mysqli_tablename($res, $row);
  }

  public function seek($res, $pos) {
    if (! $ret = @mysqli_data_seek($res, $pos)) {
      throw new \Dge\DBException("Error in Seek, MySql Error: " . $this->error());
    }
    return $ret;
  }

  public function free_result($res) {
    @mysqli_free_result($res);
  }

  public function has_table($table) {
    $this->check();
    return @mysqli_query("desc $table", $this->db);
  }

  public function drop_db($db) {
    $this->check();
    return @mysqli_drop_db($db, $this->db);
  }

  public function create_db($db) {
    $this->check();
    $ret = mysqli_create_db($db, $this->db);
    return $ret;
  }
  /*--------------------------- DEBUGGING ------------------------------------*/
  
  private function logQuery($sql, $start) {
    $query = array(
        'sql' => $sql,
        'time' => ($this->getTime() - $start)*1000
    );
    $this->queryCount ++;
    array_push($this->queries, $query);
  }
  
  private function getTime() {
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $start = $time;
    return $start;
  }
  
  private function getReadableTime($time) {
    $ret = $time;
    $formatter = 0;
    $formats = array('ms', 's', 'm');
    if($time >= 1000 && $time < 60000) {
            $formatter = 1;
            $ret = ($time / 1000);
    }
    if($time >= 60000) {
            $formatter = 2;
            $ret = ($time / 1000) / 60;
    }
    $ret = number_format($ret,3,'.','') . ' ' . $formats[$formatter];
    return $ret;
  }
}