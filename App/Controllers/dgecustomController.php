<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;


class dgecustomController extends \Dge\Controller {

  // Determine the method to use
  function __construct($params, $ct, $reg) {
    parent::__construct($ct, $reg);
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
      case 'GET':
      case 'POST':
      case 'PUT':
        if($method === 'PUT'){
          $method = 'POST';
        }
        $task = 'index';
        if(count($params) > 1 && $params[0] === 'customer'){
          $task = 'customer';
        }
        break;
      case 'DELETE':
        $task = 'index';
        break;
      default:
        # code...
        $task = '';
        break;
    }
    $this->handleTask($task, $params, $method);
  }

  private function handleTask($task, $params, $method = 'GET'){
    switch ($task.'_'.$method) {
      case 'customer_POST':
      case 'customer_PUT':
        if(count($params) !== 2){
          \Dge\Error::write($this,'E400', 'Missing customer_id');
          die;
        }
        $customer_id = intval($params[1]);
        if(!$customer_id || $customer_id < 1 ){
          \Dge\Error::write($this,'E400', 'Missing valid customer_id');
          die;
        }
        $data = $this->getPostData();
        // We need POST data
        if(!$data){
          \Dge\Error::write($this,'E400', 'Posted data is not readable');
        }
        if(isset($data['onaccount'])){
          $value = intval($data['onaccount']);
          if($value > 0){
	    // nothing
	  } else {
            $value = 0;
	  } 
        }else{
          $value = 0;
        }
        $customer = \Dge\App::loadModel('customer');
        $customer->setOnAccount($customer_id, $value);
        $this->writeHeaders();
        $res = Array();
        $res['status'] = 'ok';
        $this->write($res, 'customer');
        die;
        break;
      case 'customer_GET':
        if(count($params) !== 2){
          \Dge\Error::write($this,'E400', 'Missing customer_id');
          die;
        }
        $customer_id = intval($params[1]);
        if(!$customer_id || $customer_id < 1 ){
          \Dge\Error::write($this,'E400', 'Missing valid customer_id');
          die;
        }
        $customer = \Dge\App::loadModel('customer');
        $onaccount = $customer->getOnAccount($customer_id);
        if(count($onaccount) === 0){
          \Dge\Error::write($this,'E404', 'Not found');
        }
        $this->writeHeaders();
        $res = Array();
        $res['onaccount'] = $onaccount;
        $this->write($res, 'customer');
        die;
        break;

      default:
       \Dge\Error::write($this,'E501', 'Not Implemented');
        break;
    }
  }
}
