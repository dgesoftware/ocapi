<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;

class indexController extends Controller {

  // Determine the method to use
  function __construct($params, $ct, $reg) {
    parent::__construct($ct,$reg);

    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
      case 'GET':

        if(count($params)>0){
          $task = array_shift($params);
        }else{
          $task = 'index';
        }
        break;

      default:
        # code...
        $task = '';
        break;
    }

    $this->handleTask($task, $params, $method);
  }

  private function handleTask($task, $params, $method = 'GET'){

    switch ($task) {
      case 'index':
        # code...
        $settings = \Dge\App::getSettings();
        $endpoint = \Dge\App::getEndpoint($settings['api_dir']);
        $res = array('ep'=>$endpoint,'oc_version'=>VERSION, 'api_version'=>API_VERSION);
        $ean = isset($_REQUEST['ean'])? filter_var($_REQUEST['ean'], FILTER_SANITIZE_NUMBER_INT) : 0;
        if(intval($ean) > 0){
          // Convert an EAN code to opencart_id's
          $catalogus = \Dge\App::loadModel('product');
          $res = $catalogus->getOnEan($ean);
          $this->writeHeaders();
          $this->write($res, 'ocid');
          break;
        }
        $tst = $this->__get('acl');
        if($tst){
          // get tax_class_id's
          $catalogus = \Dge\App::loadModel('product');
          $high = $catalogus->taxRate2TaxClassId('high');
          $low = $catalogus->taxRate2TaxClassId('low');
          $none = $catalogus->taxRate2TaxClassId('none');
          $res['tax_class'] =  array('high' => $high, 'low' => $low, 'none' => $none );
          $cm = $catalogus->getLengthClass('cm');
          $mm = $catalogus->getLengthClass('mm');
          $in = $catalogus->getLengthClass('in');
          $res['length_class'] =  array('cm' => $cm, 'mm' => $mm, 'in' => $in );
          $kg = $catalogus->getWeightClass('kg');
          $g = $catalogus->getWeightClass('g');
          $lb = $catalogus->getWeightClass('lb');
          $oz = $catalogus->getWeightClass('oz');
          $res['weight_class'] =  array('kg' => $kg, 'g' => $g, 'lb' => $lb, 'oz' => $oz );
          // getWeightClass
          $order = \Dge\App::loadModel('order');
          $pending =  $order->getStatus('pending');
          $paid =  $order->getStatus('paid');
          $processing =  $order->getStatus('processing');
          $complete =  $order->getStatus('complete');
          $canceled =  $order->getStatus('canceled');
          $res['order_status'] =  array('pending' => $pending, 'paid' => $paid, 'processing' => $processing, 'complete' => $complete, 'canceled' => $canceled );
        }
        $this->writeHeaders();
        $this->write($res, 'service');
        break;
      default:
        \Dge\Error::write($this,'E501', 'Not Implemented');
        break;
    }
  }

}
