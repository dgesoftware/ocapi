<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Dge;

class productController extends \Dge\Controller {

  // Constructor($params, $ct, $reg)
  // Inherits @see Controller.php
  // * $params array exploded url '/'
  // * $ct string content-type
  // * $reg, Object opencart registry object
  //
  // This will handle any incoming request and defines
  // its purpose/method
  function __construct($params, $ct, $reg) {
    parent::__construct($ct, $reg);
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
      case 'GET':
      case 'POST':
      case 'PUT':
        if($method === 'PUT'){
          $method = 'POST';
        }
        $task = 'index';
        if(count($params) === 1 && $params[0] === 'withOptions'){
          $method = strtoupper(array_shift($params));
        }
        if(count($params) === 2 && $params[0] === 'delete'){
          $method = strtoupper(array_shift($params));
        }
        if(count($params) === 2 && $params[0] === 'copy'){
          $method = strtoupper(array_shift($params));
        }
        if($method === 'GET' && count($params) === 2 && $params[0] === 'byref'){
          $task = strtoupper(array_shift($params));
        }
        break;
      case 'DELETE':
        $task = 'index';
        break;
      default:
        $task = '';
        break;
    }
    $this->handleTask($task, $params, $method);
  }

  private function handleTask($task, $params, $method = 'GET'){
    switch ($task.'_'.$method) {
      case 'BYREF_GET':
        $id = $this->getIdParam($params);
        $result = array();
        if(!$id || $id < 1){
          \Dge\Error::write($this,'E404');
          die;
        }
        $catalogus = \Dge\App::loadModel('product');
        $res = $catalogus->getReferenceByCatId($id);

        for($i=0;$i<count($res);$i++){
          $product = $this->getproduct($res[$i]['product_id']);
          if($product){
            $result[] = $product;
          }
        }
        if(count($result) > 0){
          $this->writeHeaders();
          $this->write($result, 'product');
          die;
        }else{
          \Dge\Error::write($this,'E404');
          die;
        }
        // An array
        break;
      case 'index_GET':
        if(count($params) >= 1 && $params[0] !== ''){
          $id = $this->getIdParam($params);
          $this->get($id);
          die;
        }
        // opencart/ocapi/product.xml?limit=100&start=0
        $this->load->model('catalog/product');
        $filter = array();
        isset($_REQUEST['start']) ? $filter['start'] = (int)$_REQUEST['start'] : $filter['start'] = 0;
        isset($_REQUEST['limit']) ? $filter['limit'] = (int)$_REQUEST['limit'] : $filter['limit'] = 20;
        $filter['sort'] = 'date_modified';
        $data = $this->model_catalog_product->getProducts($filter);
        $this->writeHeaders();
        $this->write($data, 'product');
        die;
        break;
      case 'index_POST':
      case 'index_PUT':
        // We need a dge_id
        $id = $this->getIdParam($params);
        $data = $this->getPostData();
        // We need POST data
        if(!$data){
          \Dge\Error::write($this,'E400', 'Posted data is not readable');
        }
        $this->upsert($id, $data);
        break;
      case 'index_COPY':
        $id = $this->getIdParam($params);
        if(!$id || $id < 1){
          \Dge\Error::write($this,'E404');
          die;
        }
        $data = $this->getPostData();
        // We need POST data
        if(!$data){
          $data = array();
        }
        $this->copy($id, $data);
        break;
      case 'index_DELETE':

        $id = $this->getIdParam($params);
        $this->load->model('catalog/product');
        // We need a dge_id as param
        $res = $this->delete($id);
        if($res){
          $this->setResponseStatus('200 Ok');
          $this->writeHeaders();
          $this->write('Deleted', 'status');
        }else{
          \Dge\Error::write($this,'E404');
        }
        break;
      case 'index_WITHOPTIONS':
        // http://.../ocapi/product/withOptions
        // generatie a small list with products with options
        $this->productsWithOptions();
        break;
      default:
        \Dge\Error::write($this,'E501', 'Not Implemented');
        break;
    }
  }

  private function productsWithOptions(){
    $catalogus = \Dge\App::loadModel('product');
    $res = $catalogus->getProductsWithOptions();
    if(count($res)> 0){
      $this->writeHeaders();
      $this->write($res, 'products');
      die;
    }else{
      \Dge\Error::write($this,'E404');
    }
  }

  private function get($id){
    $product = $this->getproduct($id);
    if(!$product){
      \Dge\Error::write($this,'E404');
      die;
    }
    $this->writeHeaders();
    $this->write($product, 'product');
    die;
  }

  private function upsert($id, $data){
  	
  	if(is_array($data['isbn'])){
  		 $data['isbn'] = '';
  	}	
    $status = '200 Ok';
    $new = false;
    list($field_error, $data) = $this->validateAndTransform($data);
    if($field_error){
      \Dge\Error::write($this,'E001',$field_error);
    }
    $this->load->model('catalog/product');
    if(!$id){
      // We need a MINIMAL insert
      // After that we merge the data
      if(!isset($data['model']) ){
        \Dge\Error::write($this,'E002','model');
      }
      if(!isset($data['order_decimal']) ){
        $data['order_decimal'] = 0;
      }
      if(! isset($data['product_description'])){
        \Dge\Error::write($this,'E002','product_description');
      }
      if(! isset($data['price']) || (float)$data['price'] <= 0){
      }
      if(! isset($data['catalogus_id']) ){
        \Dge\Error::write($this,'E002','catalogus_id');
      }
      if(! isset($data['product_store']) ){
        $data['product_store'] = array();
        $data['product_store'][] = '0';
      }
      $insert_data = $this->dataTemplate($data);
      // Make sure every language has a 'name'
      $pdesc = $this->stranslateLocaleFromId($data['product_description']);
      if(! isset($pdesc['nl']) || ! isset($pdesc['nl']['name'])){
        \Dge\Error::write($this,'E002','product_description - nl - name');
      }
      if(! isset($pdesc['nl']['meta_title'])){
        $pdesc['nl']['meta_title'] = $pdesc['nl']['name'];
      }
      if(!isset($pdesc['nl']['meta_keyword'])){
          $pdesc['nl']['meta_keyword'] = '';
      }
      if(!isset($pdesc['nl']['meta_description'])){
        $pdesc['nl']['meta_description'] = '';
      }
      if(!isset($pdesc['nl']['description'])){
        $pdesc['nl']['description'] = '';
      }
      if(!isset($pdesc['nl']['tag'])){
        $pdesc['nl']['tag'] = '';
      }
      $insert_data['product_description'] = $this->CopyDescriptions($pdesc['nl']);
      if(! isset($data['product_category'])){
        \Dge\Error::write($this,'E002','product_category');
      }
      $insert_data['product_store'] = $this->getStoresOnCats($data['product_category']);
      if(count($insert_data['product_store']) < 1){
        \Dge\Error::write($this,'E001','product_category :no valid stores found');
      }
      if(!isset($insert_data['status'])){
        // enable the product
        // by default when status is not set
        $data['status'] = 1;
      }
      $id = $this->model_catalog_product->addProduct($insert_data);

      if(!$id){
        \Dge\Error::write($this,'E500','Error creating: new product');
      }
      // We need to ADD unit_description, unit_id, catalogus_id  in a seperate table:
      $product_id = intval($id);
    }
    $product = $this->model_catalog_product->getProduct($id);
    if($product === False){
      \Dge\Error::write($this,'E404');
       die;
    }
    $pdesc = false;
    if(isset($data['product_description'])){
      $pdesc = $this->stranslateLocaleFromId($data['product_description']);
    }
    if( $pdesc && isset($data['catalogus_id']) && isset($data['unit_id']) && isset($pdesc['nl']['unit_description'])){
        // overwrite meta data when available
        // else ignore it
        $unit_id = intval($data['unit_id']);
        $unit_description = $pdesc['nl']['unit_description'];
        $catalogus_id = intval($data['catalogus_id']);
        $catalogus = \Dge\App::loadModel('product');
        $catalogus->addDgeReference($id, $catalogus_id, $unit_id, $unit_description);
    }
    $product['product_description'] = $this->model_catalog_product->getProductDescriptions($id);
    $product = array_merge($product,
      array('product_attribute' => $this->model_catalog_product->getProductAttributes($id))
    );
    if(!isset($data['product_discount'])){
      $product = array_merge($product,
        array('product_discount' => $this->model_catalog_product->getProductDiscounts($id))
      );
    }
    if(!isset($data['product_filter'])){
      // Overwrite product filters
      $product = array_merge($product,
        array('product_filter' => $this->model_catalog_product->getProductFilters($id))
      );
    }
    $product = array_merge($product,
      array('product_image' => $this->model_catalog_product->getProductImages($id))
    );
    $product = array_merge($product,
      array('product_option' => $this->model_catalog_product->getProductOptions($id))
    );
    $product = array_merge($product,
      array('product_related' => $this->model_catalog_product->getProductRelated($id))
    );
    $product = array_merge($product,
      array('product_reward' => $this->model_catalog_product->getProductRewards($id))
    );
    if(!isset($data['product_special'])){
      $product = array_merge($product,
        array('product_special' => $this->model_catalog_product->getProductSpecials($id))
      );
    }
    if(!isset($data['product_category'])){
      $product = array_merge($product,
        array('product_category' => $this->model_catalog_product->getProductCategories($id))
      );
    }
    $product = array_merge($product,
      array('product_download' => $this->model_catalog_product->getProductDownloads($id))
    );
    $product = array_merge($product,
      array('product_layout' => $this->model_catalog_product->getProductLayouts($id))
    );
    $product = array_merge($product,
      array('product_store' => $this->model_catalog_product->getProductStores($id))
    );
    $product = array_merge($product,
      array('product_recurrings' => $this->model_catalog_product->getRecurrings($id))
    );
    
    if (empty($this->model_catalog_product->getProductSeoUrls($id)) ){
       $langs = $this->config->get('languages');
       if ($data['product_seo_url']!= ''){
       $data['product_seo_url'][0][intval($langs['nl-nl']['language_id'])] .= "_" . $id ; 
       }
    } else {	
       $data['product_seo_url'] = $this->model_catalog_product->getProductSeoUrls($id);
    }
    // This should be BEFORE any INSERTS
    // Collect images
    if( isset($data['product_image_b64']) && count($data['product_image_b64']) > 0 ){
      $images = $this->safeImages($id, $data['product_image_b64']);
      if($images && count($images)>0){
        $data['product_image'] = $images;
      }else{
        $this->setResponseStatus('406 Not Acceptable');
        $this->writeHeaders();
        $this->write('product_image_b64 contains not valid images', 'error');
      }
      unset($data['product_image_b64']);
    }
    if(isset($data['product_image_b64']) && count($data['product_image_b64']) == 0 ){
      unset($data['product_image_b64']);
      $data['product_image'] = array();
    }
    if(isset($data['image_b64'])){
      $img = $this->safeImages($id, $data['image_b64']);
      if($img && count($img) == 1){
        $data['image'] = $img[0]['image'];
      }
      unset($data['image_b64']);
    }
    $data = $this->mergeData($product, $data);
    $this->model_catalog_product->editProduct($id, $data);
    $result = array();
    $result['status'] = 'ok';
    $result['product_id'] = $id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'product');
  }

  // Clone content based on product_id
  // return a new ID, additional data
  // we copy all except the dge reference data
  private function copy($id, $data){
    $status = '200 Ok';
    $new = false;
    list($field_error, $data) = $this->validateAndTransform($data);
    if($field_error){
      \Dge\Error::write($this,'E001',$field_error);
    }
    $this->load->model('catalog/product');
    $product = $this->model_catalog_product->getProduct($id);
    if($product === False){
      \Dge\Error::write($this,'E404');
       die;
    }
    $product['product_description'] = $this->model_catalog_product->getProductDescriptions($id);
    $product = array_merge($product,
      array('product_attribute' => $this->model_catalog_product->getProductAttributes($id))
    );
    $product = array_merge($product,
      array('product_filter' => $this->model_catalog_product->getProductFilters($id))
    );
    $product = array_merge($product,
      array('product_image' => $this->model_catalog_product->getProductImages($id))
    );
    $product = array_merge($product,
      array('product_option' => $this->model_catalog_product->getProductOptions($id))
    );
    $product = array_merge($product,
      array('product_related' => $this->model_catalog_product->getProductRelated($id))
    );
    $product = array_merge($product,
      array('product_reward' => $this->model_catalog_product->getProductRewards($id))
    );
    $product = array_merge($product,
      array('product_category' => $this->model_catalog_product->getProductCategories($id))
    );
    $product = array_merge($product,
      array('product_download' => $this->model_catalog_product->getProductDownloads($id))
    );
    $product = array_merge($product,
      array('product_layout' => $this->model_catalog_product->getProductLayouts($id))
    );
    $product = array_merge($product,
      array('product_store' => $this->model_catalog_product->getProductStores($id))
    );
    $product = array_merge($product,
      array('product_recurrings' => $this->model_catalog_product->getRecurrings($id))
    );
    // This should be BEFORE any INSERTS
    if(isset($data['product_image_b64'])){
      $images = $this->safeImages($id, $data['product_image_b64']);
      if($images && count($images)>0){
        $data['product_image'] = $images;
      }else{
        $this->setResponseStatus('406 Not Acceptable');
        $this->writeHeaders();
        $this->write('product_image_b64 contains not valid images', 'error');
      }
      unset($data['product_image_b64']);
    }
    if(isset($data['image_b64'])){
      $img = $this->safeImages($id, $data['image_b64']);
      if($img && count($img) == 1){
        $data['image'] = $img[0]['image'];
      }
      unset($data['image_b64']);
    }
    $data = $this->mergeData($product, $data);
    $new_id = $this->model_catalog_product->addProduct($data);
    $pdesc = false;
    if(isset($data['product_description'])){
      $pdesc = $this->stranslateLocaleFromId($data['product_description']);
    }
    if( $new_id && $pdesc && isset($data['catalogus_id']) && isset($data['unit_id']) && isset($pdesc['nl']['unit_description'])){
        // overwrite meta data
        $catalogus = \Dge\App::loadModel('product');
        $unit_id = intval($data['unit_id']);
        $unit_description = $pdesc['nl']['unit_description'];
        $catalogus_id = intval($data['catalogus_id']);
        $catalogus = \Dge\App::loadModel('product');
        // When something changes it is suposed to be a new product
        $catalogus->addDgeReference($new_id, $catalogus_id, $unit_id, $unit_description);
    }
    $result = array();
    $result['status'] = 'ok';
    $result['product_id'] = $new_id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'product');

  }

  // ### delete($did);
  // Delete a product based on id
  private function delete($id){
    $this->load->model('catalog/product');
    $p = $this->model_catalog_product->getProduct($id);
    if(!$p){
      // Not found
      \Dge\Error::write($this,'E404');
    }
    $this->load->model('catalog/product');
    // OC model delete
    $this->model_catalog_product->deleteProduct($id);
    $catalogus = \Dge\App::loadModel('product');
    $catalogus->delDgeReference($id);
    $this->deleteImages($id);
    $result['status'] = 'deleted';
    $result['product_id'] = $p;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'product');
  }

  // If we have no stores, we don't have valid CATS either
  // we need CATS on stores to publish a product
  public function getStoresOnCats($cats){
    $this->load->model('catalog/category');
    $res = array();
    for($i = 0; $i < count($cats); $i++){
      $data = $this->model_catalog_category->getCategoryStores($cats[$i]);
      for($ii = 0; $ii < count($data);$ii++){
        if(!in_array($data[$ii], $res)){
          $res[] = $data[$ii];
        }
      }
    }
    return $res;
  }

  # Create a data template for a minimal product
  # needed for insertion
  function dataTemplate($data){
    $default_stock_id = 0;
    $settings = \Dge\App::getSettings();
    if(array_key_exists('default_stock_status_id',$settings)){
      $default_stock_id = (int)$settings['default_stock_status_id'];
    }
    $tpl = array();
    $tpl['model'] = $data['model'];
    $tpl['product_description'] = $data['product_description'];
    $tpl['product_category'] = isset($data['product_category']) ? $data['product_category'] : null;
    $tpl['product_store'] = $data['product_store'] || 0;
    $tpl['sku'] = '';
    $tpl['ean'] = '';
    $tpl['jan'] = '';
    $tpl['mpn'] = '';
    $tpl['upc'] = '';
    $tpl['isbn'] = '';
    $tpl['location'] = '';
    $tpl['quantity'] = '0';
    $tpl['minimum'] = '1';
    $tpl['decimal_order'] = '0';
    // $tpl['image'] = 'no_image.jpg';
    $tpl['subtract'] = '1';
    $tpl['stock_status_id'] = $default_stock_id; // ?
    $tpl['manufacturer_id'] = '';
    $tpl['shipping'] = '1';
    $tpl['price'] = isset($data['price']) ? $data['price'] : 0;
    $tpl['unit_id'] = 0;
    $tpl['enable_matrix'] = 0;
    $tpl['catalogus_id'] = 0;
    $tpl['matrix_group_id'] = 0;
    $tpl['date_available'] =  date( 'Y-m-d H:i:s', time());
    $tpl['points'] = '0';
    $tpl['weight'] = '0';
    $tpl['weight_class_id'] = '0';
    $tpl['length'] = '0';
    $tpl['width'] = '0';
    $tpl['height'] = '0';
    $tpl['length_class_id'] = '0';
    $tpl['length'] = '0';
    $tpl['status'] = '1';
    $tpl['tax_class_id'] = '0';
    $tpl['sort_order'] = '0';
    $tpl['keyword'] = '';
    return $tpl;
  }

  function validateArray($field, $arr){
    $res = true;
    for($i = 0; $i < count($arr); $i++){
      $res = $this->validate($field, $arr[$i]);
      if(!$res){
        return false;
      }
    }
    return true;
  }


  // Validate on all values we receive from a POST
  // convert when needed
  function validateAndTransform($data){
    $res = true;
    // These values are managed via the '_b64' tag
    if(isset($data['image'])){
      unset($data['image']);
    }
    if(isset($data['product_image'])){
      unset($data['product_image']);
    }
     // These values are for internal use only
    // and are managed via the OpenCArt interface
    if(isset($data['product_id'])){
      unset($data['product_id']);
    }
    if(isset($data['product_option'])){
      unset($data['product_option']);
    }
    if(isset($data['product_download'])){
      unset($data['product_download']);
    }
    if(isset($data['weight']) && \Dge\App::is_assoc($data['weight'])){
      $data = $this->convertWeight($data);
    }else{
      unset($data['weight']);
    }
    if(isset($data['dimensions']) && \Dge\App::is_assoc($data['dimensions'])){
      $data = $this->convertDimensions($data);
    }else{
      unset($data['dimensions']);
    }
    if(isset($data['product_discount'])){
      $data = $this->cleanupProductDiscount($data);
    }
    if(isset($data['product_attribute'])){
      $data = $this->cleanupProductAttribute($data);

    }
    if(isset($data['product_special'])){
      $data = $this->cleanupProductSpecial($data);
    }

    // Convert a tax indication (high, low, none)
    // to a tax_class_id
    // should be found in the settings.json
    if(isset($data['tax'])){
      $catalogus = \Dge\App::loadModel('product');
      $val = $catalogus->taxRate2TaxClassId($data['tax']);
      if($val === false){
        return array('tax : make sure the settings.json tax keyword matches a valid tax_class_id', false);
      }else{
        $data['tax_class_id'] = $val;
        unset($data['tax']);
      }
    }
    foreach ($data as $key => $value) {
      if( ( is_string($value) || is_numeric($value))
          // This fields should be arrays
          // (we can have multiple values)
          && (
               $key === 'product_category'
            || $key === 'product_store'
            || $key === 'product_image_b64'
            || $key === 'product_seo_url'
          )
      ){
	$temp = $key;      
	if ($temp === 'product_seo_url') {
           $langs = $this->config->get('languages');		
	   $help[intval($langs['nl-nl']['language_id'])] = $value;
	   $data[$key] = array($help);	
	} else {     	
           $data[$key] = array($value);
	   $value =  $data[$key];
	}
      }
      $field = $key;
      if ($field === 'product_category'){
        $field = 'category_id';
	if(is_numeric($data[$key])){
          $data[$key] = array($data[$key]);
        }
	$data[$key] = array_unique($data[$key]);
      }
      if ($field === 'product_seo_url'){
        if(is_string($data[$key])){
           $langs = $this->config->get('languages');
       	   $help[intval($langs['nl-nl']['language_id'])] = $data[$key];  	   
 	   $data[$key] = array($help);
        }
	$data[$key] = array_unique($data[$key]);
      }
      if ($field === 'product_related'){
        $field = 'product_id';
        if(is_numeric($data[$key])){
          $data[$key] = array($data[$key]);
        }
        $data[$key] = array_unique($data[$key]);
      }
      if ($field === 'product_filter'){
        $field = 'filter_id';
        if(is_numeric($data[$key])){
          $data[$key] = array($data[$key]);
        }
        $data[$key] = array_unique($data[$key]);
      }
      if($key === 'product_discount'){
        if(!is_array($value)){
          return array($key, false);
        }
        $err = $this->productDiscountCheck($value);
        if($err){
          return array( 'product_discount : '.$err, false);
        }else{
          $res = true;
        }
      }else if($key === 'product_attribute'){
         $res = true;
      }else if($key === 'product_special'){
        if(!is_array($value)){
          return array($key, false);
        }
        $err = $this->productSpecialCheck($value);
        if($err){
          return array( 'product_special : '.$err, false);
        }else{
          $res = true;
        }
      }else if(is_array($value) && ! \Dge\App::is_assoc($value) ){
        // Value is an indexed array
        // check every single value
        $res = $this->validateArray($field, $value);
        if(!$res){
          return array($key, false);
        }
      }else if($key === 'product_description'){
        list($err, $new_ar) = $this->stranslateLocaleFromISO2($value);
        if($err){
          return array( 'product_description : '.$err, false);
        }else{
          $data[$key] = $new_ar;
          $res = true;
        }
      }else{
        $res = $this->validate($field, $value);
      }
      if(!$res){
        return array($key, false);
      }
    }
    return array(false, $data);
  }

  # return TRUE on success
  function validate($type, $value){

    $res = false;
    if(!(is_string($value) || is_numeric($value))){
      # only check string or nummeric values
      return false;
    }

    switch ($type) {
      // Ints
      case 'product_id':
      case 'quantity':
      case 'minimum':
      case 'stock_status_id':
      case 'manufacturer_id':
      case 'points':
      case 'weight_class_id':
      case 'length_class_id':
      case 'sort_order':
      case 'language_id':
      case 'tax_class_id':
      case 'store_id':
      case 'attribute_id':
      case 'option_id':
      case 'product_option_id':
      case 'product_option_value_id':
      case 'option_value_id':
      case 'customer_group_id':
      case 'points':
      case 'priority':
      case 'download_id':
      case 'category_id':
      case 'filter_id':
      case 'related_id':
      case 'layout_id':
      case 'profile_id':
      case 'unit_id':
      case 'catalogus_id':
      case 'customer_id':
         $res = (int)$value.'' == $value.'';
         break;
      // floats
      case 'price':
      case 'weight':
      case 'length':
      case 'width':
      case 'height':
        $res = is_numeric($value);
        break;
      // tiny ints
      case 'subtract':
      case 'shipping':
      case 'status':
      case 'required':
      case 'decimal_order':

        $res = (
          is_numeric($value)
          && is_integer((int)$value)
          && ( (int)$value == 0 || (int)$value == 1) );
        break;
      case 'tax':
        $res = (is_string($value) && ($value=='high' || $value=='low' || $value=='none'));
        break;
      case 'price_prefix':
      case 'points_prefix':
      case 'weight_prefix':

        $res = (strlen($value.'') < 2 );
        break;
      case 'model':
        $res = (strlen($value.'') > 0 && strlen($value) < 64);
        break;
      case 'tax':
      case 'location':
        $res = (strlen($value.'') < 64);
        break;
      case 'sku':
      case 'mpn':
        $res = (strlen($value.'') < 65);
        break;
      case 'upc':
        $res = (strlen($value.'') < 13);
        break;
      case 'ean':
        $res = (strlen($value.'') < 129);
        break;
      case 'jan':
      case 'isbn';
        $res = (strlen($value.'') < 14);
        break;
      case 'image':
      case 'keyword':
      case 'meta_description':
      case 'meta_keyword':
      case 'unit_description':
        $res = (strlen($value.'') < 256);
        break;
      case 'name':
      case 'meta_title':
        $res = (strlen($value.'') > 2 && strlen($value.'') < 101);
        break;
      case 'description':
      case 'product_seo_url':
      case 'tag':
      case 'text':
      case 'option_value':
      case 'image_b64':
      case 'product_image_b64':
        $res = (is_string($value) || is_numeric($value));
        break;
      case 'created':
      case 'date_added':
      case 'date_modified':
      case 'date_start':
      case 'date_end':
        // yyyy-mm-dd
        $pattern = '/^\d{4}-\d{2}-\d{2}$/';
        $test = preg_match ( $pattern , $value );
        if($test === 1){
          $res = true;
        }else{
          $res = false;
        }
        break;
      default:
        # code...
        break;
    }
    return $res;
  }

  // ##### safeImages($dge_id, $product_id, $arr)
  // convert an array containing base64 image data
  // to an image and return an array of objects
  // ready for OC to insert
  public function safeImages($product_id, $arr){

    $main_image = false;
    if(is_string($arr)){
      // THIS IS THE MAIN PRODUCT IMAGE
      // WE JUST OVERWRITE THE PREVIOUS ONE
      // The filename is something like: data/p/[$product_id]_0.jpg
      $main_image = true;
      $arr = array($arr);
      $f_suffix = 0;
    }else{
      // Sub images start with 1
      $f_suffix = 1;
    }
    $bina = array();
    $sort_order = 0;
    $directory = DIR_IMAGE . 'catalog/p';
    if(!is_dir($directory)){
      mkdir($directory, 0777);
    }
    $subdir = substr($product_id.'000', 0, 3);
    $directory = $directory.'/'.$subdir;
    if(!is_dir($directory)){
      mkdir($directory, 0777);
    }
    require_once(DGE_APP_PATH.'/Helpers/Binary.php');
    for($i=0;$i<count($arr);$i++){
      list($ct , $data) = \Dge\Binary::b64ToBin($arr[$i]);
      if($ct === 'image/jpeg'){
        $ext = '.jpg';
      }elseif($ct === 'image/webp'){
        $ext = '.webp';
      }else{
        $ext = '.png';
      }
      // Not a valid image?
      // Refuse the rest as well
      if($ct && $data){
        $filename = $product_id.'_'.$f_suffix.$ext;
        $image = 'catalog/p/'.$subdir.'/'.$filename;
        // Just write, leave it untouched
        $fp = fopen($directory.'/'.$filename, 'w');
        fwrite($fp, $data);
        fclose($fp);
        $bina[] = array('image' => $image,
                      'sort_order' => $sort_order,
                      'product_id' => $product_id
                      );
        $f_suffix ++;
        $sort_order ++;
      }
    }
    return $bina;
  }

  // Delete ALL product images
  public function deleteImages($id){
    $directory = DIR_IMAGE . 'catalog/p';
    $subdir = substr($id.'000', 0, 3);
    $directory = $directory.'/'.$subdir;
    if(!is_dir($directory)){
      return true;
    }
    $files = glob($directory. '/'.$id.'_*');
    if ($files) {
      foreach ($files as $file) {
        unlink ( $file );
      }
    }
    return true;
  }

  public function convertWeight($data){
    // g. kgs
    $weight_class_id = NULL;
    $find = strtolower($data['weight']['weight_class']);
    $this->load->model('localisation/weight_class');
    $classes = $this->model_localisation_weight_class->getWeightClasses();
    for($i=0;$i<count($classes);$i++){
      if($find === strtolower($classes[$i]['unit'])){
        $weight_class_id = $classes[$i]['weight_class_id'];
        break;
      }
    }
    if($weight_class_id){
      $data['weight_class_id'] = $weight_class_id;
      $data['weight'] = $data['weight']['weight'];
    }
    return $data;
  }

  // Data is an array
  public function convertDimensions($data){
    $length_class_id = NULL;
    $find = strtolower($data['dimensions']['length_class']);
    $this->load->model('localisation/length_class');
    $classes = $this->model_localisation_length_class->getLengthClasses();
    for($i=0;$i<count($classes);$i++){
      if($find === strtolower($classes[$i]['unit'])){
       $length_class_id = $classes[$i]['length_class_id'];
        break;
      }
    }
    if($length_class_id){
      $data['length_class_id'] = $length_class_id;
      $data['length'] = $data['dimensions']['length'];
      $data['width'] = $data['dimensions']['width'];
      $data['height'] = $data['dimensions']['height'];
      unset($data['dimensions']);
    }
    // Just return the data record
    return $data;
  }

  // set date_start end date_end (when missing)
  // convert to array when assoc
  public function cleanupProductDiscount($data){
    if(\Dge\App::is_assoc($data['product_discount'])){
      // convert to plain array
      $data['product_discount'] = array($data['product_discount']);
    }
    // set an empty start and end date when not givven
    for($i=0; $i<count($data['product_discount']); $i++){
      if(!isset($data['product_discount'][$i]['priority']) ){
        $data['product_discount'][$i]['priority'] = 0;
      }
      if(!isset($data['product_discount'][$i]['date_start'])){
        $data['product_discount'][$i]['date_start'] = '0000-00-00';
      }
      if(!isset($data['product_discount'][$i]['date_end'])){
        $data['product_discount'][$i]['date_end'] = '0000-00-00';
      }
    }
    return $data;
  }

  private function productDiscountCheck($arr){
    for($i=0;$i<count($arr);$i++){
      if(!\Dge\App::is_assoc($arr[$i])){
        return 'Invalid product_discount rule';
      }
      if(!isset($arr[$i]['customer_group_id'])){
        return 'Missing: customer_group_id';
      }
      if(!isset($arr[$i]['quantity'])){
        return 'Missing: quantity';
      }
      if(!isset($arr[$i]['priority'])){
        return 'Missing: priority';
      }
      if(!isset($arr[$i]['price'])){
        return 'Missing: price';
      }
      foreach ($arr[$i] as $key => $value) {
        $res = $this->validate($key, $value);
        if(!$res){
          return $key;
        }
      }
    }
    return false;
  }

  private function cleanupProductAttribute($data){
    if(\Dge\App::is_assoc($data['product_attribute'])){
      // convert to plain array
      $data['product_attribute'] = array($data['product_attribute']);
    }
    $res = array();
    for($i=0;$i<count($data['product_attribute']);$i++){
      if($data['product_attribute'][$i]['product_attribute_description']){
        list($err, $res) = $this->stranslateLocaleFromISO2($data['product_attribute'][$i]['product_attribute_description']);
        if(!$err){
          $data['product_attribute'][$i]['product_attribute_description'] = $res;
        }else{
          // unset or error? unset for now
          unset($data['product_attribute']);
        }
      }
    }
    return $data;
  }

  // set date_start end date_end (when missing)
  // convert to array when assoc
  public function cleanupProductSpecial($data){
    if(\Dge\App::is_assoc($data['product_special'])){
      // convert to plain array
      $data['product_special'] = array($data['product_special']);
    }
    // set an empty start and end date when not givven
    for($i=0; $i<count($data['product_special']); $i++){
      if(!isset($data['product_special'][$i]['customer_group_id'])){
        $data['product_special'][$i]['customer_group_id'] = 1;
      }
      if(!isset($data['product_special'][$i]['priority']) ){
        $data['product_special'][$i]['priority'] = 0;
      }
      if(!isset($data['product_special'][$i]['date_start'])){
        $data['product_special'][$i]['date_start'] = '0000-00-00';
      }
      if(!isset($data['product_special'][$i]['date_end'])){
        $data['product_special'][$i]['date_end'] = '0000-00-00';
      }
    }
    return $data;
  }

  private function productSpecialCheck($arr){
    for($i=0;$i<count($arr);$i++){
      if(!\Dge\App::is_assoc($arr[$i])){
        return 'Invalid product_discount rule';
      }
      if(!isset($arr[$i]['customer_group_id'])){
        return 'Missing: customer_group_id';
      }
      if(!isset($arr[$i]['priority'])){
        return 'Missing: priority';
      }
       if(!isset($arr[$i]['price'])){
        return 'Missing: price';
      }
      foreach ($arr[$i] as $key => $value) {
        $res = $this->validate($key, $value);
        if(!$res){
          return $key;
        }
      }
    }
    return false;
  }

  // just make it an array when string
  // ['product_related'] is an array of opencart product id's
  private function convertProductRelated($data){
    if(is_string($data['product_related'].'')){
      $data['product_related'] = array($data['product_related']);
    }
    return $data;
  }

  private function getproduct($id){
    $this->load->model('catalog/product');
    $product = $this->model_catalog_product->getProduct($id);
    if(!$product){
      return false;
      die;
    }
    $catalogus = \Dge\App::loadModel('product');
    $product['product_description'] = $this->model_catalog_product->getProductDescriptions($id);
    $product['product_description'] = $this->stranslateLocaleFromId(
        $product['product_description']
    );
    $product = array_merge($product,
      array('product_attribute' => $this->model_catalog_product->getProductAttributes($id))
    );
    $product = array_merge($product,
      array('product_discount' => $this->model_catalog_product->getProductDiscounts($id))
    );
    $product = array_merge($product,
      array('product_filter' => $this->model_catalog_product->getProductFilters($id))
    );
    $product = array_merge($product,
      array('product_image' => $this->model_catalog_product->getProductImages($id))
    );
    $product = array_merge($product,
      array('product_option' => $this->model_catalog_product->getProductOptions($id))
    );
    $maxlen = count($product['product_option']);

    for($cnt=0;$cnt<$maxlen;$cnt++) {
      $len = count($product['product_option'][$cnt]['product_option_value']);
      for($i=0;$i<$len;$i++){
        $product['product_option'][$cnt]['product_option_value'][$i]['option_value'] =
        $this->model_catalog_product->getProductOptionValue($id,$product['product_option'][$cnt]['product_option_value'][$i]['product_option_value_id'] );
      }
    }
    $product = array_merge($product,
      array('product_related' => $this->model_catalog_product->getProductRelated($id))
    );
    $product = array_merge($product,
      array('product_reward' => $this->model_catalog_product->getProductRewards($id))
    );
    $product = array_merge($product,
      array('product_special' => $this->model_catalog_product->getProductSpecials($id))
    );
    $product = array_merge($product,
      array('product_category' => $this->model_catalog_product->getProductCategories($id))
    );
    $product = array_merge($product,
      array('product_download' => $this->model_catalog_product->getProductDownloads($id))
    );
    $product = array_merge($product,
      array('product_layout' => $this->model_catalog_product->getProductLayouts($id))
    );
    $product = array_merge($product,
      array('product_store' => $this->model_catalog_product->getProductStores($id))
    );
    $product = array_merge($product,
      array('product_recurrings' => $this->model_catalog_product->getRecurrings($id))
    );
    try {
      $product = array_merge($product,
        array('dge_reference' => $catalogus->getDgeReference($id))
      );
    } catch (Exception $e){
        $product['dge_reference'] = false;
    }
    return $product;

  }
}
