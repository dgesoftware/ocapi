<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;


class customerController extends \Dge\Controller {

  // Determine the method to use
  function __construct($params, $ct, $reg) {
    parent::__construct($ct, $reg);
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
      case 'GET':
      case 'POST':
      case 'PUT':
        if($method === 'PUT'){
          $method = 'POST';
        }
        $task = 'index';
        if(count($params) === 2 && $params[0] === 'delete'){
          $method = strtoupper(array_shift($params));
        }
        break;
      case 'DELETE':
        $task = 'index';
        break;
      default:
        # code...
        $task = '';
        break;
    }
    $this->handleTask($task, $params, $method);
  }

  private function handleTask($task, $params, $method = 'GET'){
    switch ($task.'_'.$method) {
      case 'index_GET':
        $customer = \Dge\App::loadModel('customer');
        if($method === 'GET'){
          $id = $this->getIdParam($params);
          if($id){
            $this->get($id);
            die;
          }
          $this->index();
        }
        break;
      case 'index_POST':
      case 'index_PUT':
        $id = $this->getIdParam($params);
        $data = $this->getPostData();
        // We need POST data
        if(!$data){
          \Dge\Error::write($this,'E400', 'Posted data is not readable');
        }
        $this->upsert($id,$data);
        break;
      case 'index_DELETE':
        $id = $this->getIdParam($params);
        $this->delete($id);
        break;
      default:
       \Dge\Error::write($this,'E501', 'Not Implemented');
        break;
    }
  }

  private function index(){

    $customer = \Dge\App::loadModel('customer');
    $this->load->model('customer/customer');
    $filter = array();
    isset($_REQUEST['start']) ? $filter['start'] = (int)$_REQUEST['start'] : $filter['start'] = 0;
    isset($_REQUEST['limit']) ? $filter['limit'] = (int)$_REQUEST['limit'] : $filter['limit'] = 20;
    $data = $this->model_customer_customer->getCustomers($filter);
    $i = 0;
    $len = count($data);
    if($len === 0){
       $this->setResponseStatus('404 Not Found');
    }
    for($i = 0; $i < $len; $i++){
      unset($data[$i]['salt']);
      unset($data[$i]['password']);
      unset($data[$i]['cart']);
      unset($data[$i]['wishlist']);
      unset($data[$i]['token']);
      unset($data[$i]['ip']);
      // Last address is default ?
      $data[$i]['address'] = $this->model_customer_customer->getAddresses($data[$i]['customer_id']);
      $data[$i]['master'] = $customer->getMasterAccount($data[$i]['customer_id']);
    }
    $this->writeHeaders();
    $this->write($data, 'customer');
  }

  private function get($id){
    $this->load->model('customer/customer');
    $customer = \Dge\App::loadModel('customer');
    $data = $this->model_customer_customer->getCustomer($id);
    if(!$data) {
      $this->setResponseStatus('404 Not Found');
      $this->writeHeaders();
      $this->write(array(), 'customer');
      die;
    }
    // Do not leak secrets
    unset($data['salt']);
    unset($data['password']);
    unset($data['cart']);
    unset($data['wishlist']);
    unset($data['token']);
    unset($data['ip']);
    $data['master'] = $customer->getMasterAccount($data['customer_id']);
    $addresses = $this->model_customer_customer->getAddresses($data['customer_id']);
    $data['address'] =array();
    foreach($addresses as $ad){
      $data['address'][] = $ad;
    }
    $this->writeHeaders();
    $this->write($data, 'customer');
    die;
  }

  private function upsert($id, $data){
    $this->load->model('customer/customer');
    $status = '200 Ok';
    $new = false;
    unset($data['purchase_factor']);
    unset($data['discount']);
    list($field_error, $data) = $this->validateAndTransform($data);
    if($field_error){
      \Dge\Error::write($this,'E001',$field_error);
      die;
    }
    if(!$id){
      if(!isset($data['email'])){
        \Dge\Error::write($this,'E002','email');
      }else{
        $test = $this->model_customer_customer->getCustomerByEmail($data['email']);
        if($test){
          \Dge\Error::write($this,'E002','email address allready in use customer_id:'.$test['customer_id']);
        }
      }
      if(!isset($data['firstname'])){
        \Dge\Error::write($this,'E002','firstname');
      }
      if(!isset($data['lastname'])){
        \Dge\Error::write($this,'E002','lastname');
      }
      if(!isset($data['telephone'])){
        \Dge\Error::write($this,'E002','telephone');
      }
      if(!isset($data['password'])){
        \Dge\Error::write($this,'E002','password');
      }
      if(!isset($data['customer_group_id']) || intval($data['customer_group_id']) === 0 ){
        // Get the default customer_group_id
        $data['customer_group_id'] = $this->config->get('config_customer_group_id');
      }
      if(!isset($data['status'])){
        // The customer is standard enabled
        $data['status'] = 1; //
      }
      $data['safe'] = 1;
      // A customer via the ocapi is automaticly approved
      if(!isset($data['newsletter'])){
        $data['newsletter'] = 0; //
      }
      if(!isset($data['fax'])){
        $data['fax'] = ''; //
      }
      // We should check on EMAIL first
      // only ONE email adres per account
      // email is UNIQUE
      $g = \Dge\App::loadModel('customer');
      $id = $g->addCustomer($data);
      $result = array();
      $result['status'] = 'ok';
      $result['customer_id'] = $id;
      $this->setResponseStatus('200 Ok');
      $this->writeHeaders();
      $this->write($result, 'customer');
      die;
    }

    // Get clean exsisting data
    // Merge it with new data
    $old_data = $this->model_customer_customer->getCustomer($id);
    if(!$old_data) {
      $this->setResponseStatus('404 Not Found');
      $this->writeHeaders();
      $this->write(array(), 'customer');
      die;
    }
    // Do not leak secrets
    unset($old_data['salt']);
    $old_data['password'] = false;
    unset($old_data['cart']);
    unset($old_data['wishlist']);
    unset($old_data['token']);
    unset($old_data['ip']);
    $old_data['address'] = $this->model_customer_customer->getAddresses($old_data['customer_id']);
    // Merge
    $data = $this->mergeData($old_data, $data);
    $this->load->model('customer/customer');
    $data = $this->model_customer_customer->editCustomer($id, $data);
     $result = array();
    $result['status'] = 'ok';
    $result['customer_id'] = $id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'customer');
    die;
  }



  // ##### delete($id);
  // Delete a customer group
  private function delete($id){
    $this->load->model('customer/customer');
    $test = $this->model_customer_customer->getCustomer($id);
    if(!$test || $id < 1){
      \Dge\Error::write($this,'E404');
    }else{
      unset($test);
    }
    $this->model_customer_customer->deleteCustomer($id);
    $result['status'] = 'deleted';
    $result['customer_id'] = $id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'customer');
  }

  // return TRUE on success
  protected function validate($type, $value){
    $res = false;
    if(!(is_string($value) || is_numeric($value))){
      # only check string or nummeric values
      return false;
    }
    switch ($type) {
      case 'customer_id':
      case 'customer_group_id':
      case 'store_id':
      case 'language_id':
      case 'address_id':
      case 'country_id':
      case 'zone_id':
        $res = ( is_numeric($value)
                  && is_integer((int)$value)
                  && (int)$value.'' == $value
                );
        break;
      case 'purchase_factor':
        $res = is_numeric($value);
        break;
      // tiny ints
      case 'newsletter':
      case 'status':
      case 'approved':
      case 'default':
        $res = (
          is_numeric($value)
          && is_integer((int)$value)
          && ( (int)$value == 0 || (int)$value == 1) );
        break;
      case 'postcode':
        $res = (strlen($value.'') < 11);
        break;
      case 'firstname':
      case 'lastname':
      case 'telephone':
        $res = (strlen($value.'') > 0 && strlen($value.'') < 33);
        break;
      case 'password': // magento fix 6 chars for a passord
        $res = (strlen($value.'') > 5 && strlen($value.'') < 33);
        break;
      case 'email':
        $res = (strlen($value.'') > 5 && strlen($value.'') < 97);
        break;
      case 'company':
        $res = (strlen($value.'') < 41);
        break;
      case 'fax':
      case 'company_id':
      case 'tax_id':
        $res = (strlen($value.'') < 33);
        break;
      case 'address_1':
      case 'address_2':
      case 'city':
        $res = (strlen($value.'') < 129);
        break;
      case 'date_added':
      case 'date_modified':
        // yyyy-mm-dd
        $pattern = '/^\d{4}-\d{2}-\d{2}$/';
        $test = preg_match ( $pattern , $value );
        if($test === 1){
          $res = true;
        }else{
          $res = false;
        }
        break;
      default:
        # code...
        break;
    }
    return $res;
  }

  protected function validateArray($field, $arr){
    $res = true;
    for($i = 0; $i < count($arr); $i++){
      $res = $this->validate($field, $arr[$i]);
      if(!$res){
        return false;
      }
    }
    return true;
  }

  // Validate all values we receive from a POST
  // converted when needed
  protected function validateAndTransform($data){
    $res = true;
    // Cleanup data which we should NOT recieve / overwrite
    foreach ($data as $key => $value) {
      $field = $key;
      if($key == 'email'){
        // email always LOWERCASE
        $data[$key] = strtolower($value);
        $value = strtolower($value);
      }
      if($key == 'address' ){
        if(\Dge\App::is_assoc($value)){
          $data[$key] = array($value);
        }
        $value = $data[$key];
        list($err, $addr) = $this->addresCheck($data[$key]);
        if($err){
          return array($err, false);
        }else{
          $data[$key] = $addr;
          $res = true;
        }
      }else if(is_array($value) && ! \Dge\App::is_assoc($value) ){
        // Value is an indexed array
          // check every single value
          $res = $this->validateArray($field, $value);
          if(!$res){
            return array($key, false);
          }
      }else{
          $res = $this->validate($field, $value);
      }
      if(!$res){
        return array($key, false);
      }
    }
    return array(false, $data);
  }

  // Check address data
  protected function addresCheck($arr){
    for($i=0;$i<count($arr);$i++){
      if(!\Dge\App::is_assoc($arr[$i])){
        return array('Invalid address rule:'.$i , false);
      }
      if(!isset($arr[$i]['address_1'])){
        return array('Missing: address_1', false);
      }
      if(!isset($arr[$i]['firstname'])){
        return array('Missing: firstname', false);
      }
      if(!isset($arr[$i]['lastname'])){
        return array('Missing: lastname', false);
      }
      if(!isset($arr[$i]['city'])){
        return array('Missing: city', false);
      }
      if(!isset($arr[$i]['country'])){
        return array('Missing: country', false);
      }
      if(!isset($arr[$i]['postcode'])){
        return array('Missing: postcode', false);
      }
      // convertCountry to country_id
      $country = $this->getCountryByIso2($arr[$i]['country']);
      if(!$country){
        return array('Invalid: country code iso2', false);
      }else{
        unset($arr[$i]['country']);
        $arr[$i]['country_id'] = $country['country_id'];
      }
      if(isset($arr[$i]['region'])){
        $tst = $this->getZoneByName($arr[$i]['country_id'], $arr[$i]['region']);
        if($tst){
          $arr[$i]['zone_id'] = $tst['zone_id'];
        }else{
          $arr[$i]['zone_id'] = 0;
        }
        unset($arr[$i]['region']);
      }else{
        $arr[$i]['zone_id'] = 0;
      }
      // Fill defaults
      if(!isset($arr[$i]['company'])){
        $arr[$i]['company'] = '';
      }
      if(!isset($arr[$i]['fax'])){
        $arr[$i]['fax'] = '';
      }
      if(!isset($arr[$i]['company_id'])){
        $arr[$i]['company_id'] = '';
      }
      if(!isset($arr[$i]['tax_id'])){
        $arr[$i]['tax_id'] = '';
      }
      if(!isset($arr[$i]['address_2'])){
        $arr[$i]['address_2'] = '';
      }
      foreach ($arr[$i] as $key => $value) {
        $res = $this->validate($key, $value);
        if(!$res){
          return array($key, null);
        }
      }
    }
    return array(false, $arr);
  }
}
