<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;

class filtergroupController extends \Dge\Controller {
  function __construct($params, $ct, $reg) {
    parent::__construct($ct, $reg);
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
      case 'GET':
      case 'POST':
      case 'PUT':
        if($method === 'PUT'){
          $method = 'POST';
        }
        $task = 'index';
        if(count($params) === 2 && $params[0] === 'delete'){
          $method = strtoupper(array_shift($params));
        }
        break;
      case 'DELETE':
        $task = 'index';
        break;
      default:
        $task = '';
        break;
    }
    $this->handleTask($task, $params, $method);
  }

  private function handleTask($task, $params, $method = 'GET'){
    switch ($task.'_'.$method) {
      case 'index_GET':
        $this->load->model('catalog/filter');
        if($method === 'GET'){
          $filter_id = $this->getIdParam($params);
          if($filter_id){
            $this->get($filter_id);
            return;
          }else{
            $this->index();
          }
        }
        break;
      case 'index_POST':
      case 'index_PUT':
        $this->load->model('catalog/filter');
        $filter_id = $this->getIdParam($params);
        $data = $this->getPostData();
        if(!$data){
          \Dge\Error::write($this,'E400', 'Posted data is not readable');
        }
        $this->upsert($filter_id,$data);
        break;
      case 'index_DELETE':
        $filter_id = $this->getIdParam($params);
        $this->load->model('catalog/filter');
        // We need a dge_id as param
        $this->delete($filter_id);
        break;
      default:
        \Dge\Error::write($this,'E501', 'Not Implemented');
        break;
    }
  }

  private function index(){
    $filter = array();
    $res = array();
    isset($_REQUEST['start']) ? $filter['start'] = (int)$_REQUEST['start'] : $filter['start'] = 0;
    isset($_REQUEST['limit']) ? $filter['limit'] = (int)$_REQUEST['limit'] : $filter['limit'] = 20;
    $data = $this->model_catalog_filter->getFilterGroups($filter);
    $i = 0;
    $len = count($data);
    if($len === 0){
      // Not found / no result
      $this->setResponseStatus('404 Not Found');
    }else{
      for($i=0;$i<$len;$i++){
        $g = $this->model_catalog_filter->getFilterGroup($data[$i]['filter_group_id']);
        unset($g['language_id']);
        unset($g['name']);
        $g['filter_group_description'] = $this->stranslateLocaleFromId($this->model_catalog_filter->getFilterGroupDescriptions($data[$i]['filter_group_id']));
        $filters = $this->model_catalog_filter->getFilterDescriptions($data[$i]['filter_group_id']);
        $fi = 0;
        $flen = count($filters);
        $g['filter'] = array();
        for($fi=0;$fi < $flen; $fi++){
          $fres =  array();
          $fres['filter_id'] = $filters[$fi]['filter_id'];
          $fres['sort_order'] = $filters[$fi]['sort_order'];
          $fres['filter_description'] =  $this->stranslateLocaleFromId($filters[$fi]['filter_description']);
          $g['filter'][] = $fres;
        }
        $res[] = $g;
      }
    }
    $this->writeHeaders();
    $this->write($res, 'filter');
  }

  private function get($filter_id){
    $data = $this->model_catalog_filter->getFilterGroup($filter_id);
    if(!$data){
      $this->setResponseStatus('404 Not Found');
      $this->writeHeaders();
      $this->write(array(), 'filter');
      die;
    }
    unset($data['language_id']);
    unset($data['name']);
    $data['filter'] = array();
    $data['filter_group_description'] = $this->stranslateLocaleFromId($this->model_catalog_filter->getFilterGroupDescriptions($filter_id));
    $filters = $this->model_catalog_filter->getFilterDescriptions($filter_id);
    $fi = 0;
    $flen = count($filters);
    $g['filter'] = array();
    for($fi=0;$fi < $flen; $fi++){
      $fres =  array();
      $fres['filter_id'] = $filters[$fi]['filter_id'];
      $fres['sort_order'] = $filters[$fi]['sort_order'];
      $fres['filter_description'] =  $this->stranslateLocaleFromId($filters[$fi]['filter_description']);
      $data['filter'][] = $fres;
    }
    $this->writeHeaders();
    $this->write($data, 'filter');
    die;
  }

  // if $filter_id == false
  // we do an INSERT
  // ELSE an update
  private function upsert($filter_id, $data){
    $status = '200 Ok';
    $new = false;
    list($field_error, $data) = $this->validateAndTransform($data);
    if($field_error){
      \Dge\Error::write($this,'E001',$field_error);
    }
    if(!$filter_id){
      if(! isset($data['filter_group_description'])){
        \Dge\Error::write($this,'E002','filter_group_description');
      }
      $test = $this->stranslateLocaleFromId($data['filter_group_description']);
      if(! isset($test['nl']) || ! isset($test['nl']['name'])){
        \Dge\Error::write($this,'E002','filter_group_description - nl - name');
      }
      if(!isset($data['sort_order'])){
        $data['sort_order'] = 0;
      }
      if(isset($data['filter'])){
        if(!is_array($data['filter'])){
          \Dge\Error::write($this,'E002','filter not an array');
          return;
        }
        foreach ($data['filter'] as $filter) {
          if(!isset($filter['sort_order'])){
            $filter['sort_order'] = 0;
          }
          if(!isset($filter['filter_id'])){
            $filter['filter_id'] = false;
          }
          foreach ($filter['filter_description'] as $language_id => $filter_description) {
            if(! isset($filter_description['name'])){
              \Dge\Error::write($this,'E002','Missing filter filter_description name');
              return;
            }
          }
        }
      }
      $custom_filter = \Dge\App::loadModel('filter');
      $filter_id = $custom_filter->addfilter($data);
      if(!$filter_id){
        \Dge\Error::write($this,'E500','Insert filter');
      }
      $result = array();
      $result['status'] = 'ok';
      $result['filter_id'] = $filter_id;
      $this->setResponseStatus('200 Ok');
      $this->writeHeaders();
      $this->write($result, 'filter');
    }

    $old_data = $this->model_catalog_filter->getFilterGroup($filter_id);
    if(!$old_data){
      \Dge\Error::write($this,'E404');
      die;
    }
    unset($old_data['language_id']);
    unset($old_data['name']);
    $old_data['filter'] = array();
    $old_data['filter_group_description'] = $this->model_catalog_filter->getFilterGroupDescriptions($filter_id);
    $filters = $this->model_catalog_filter->getFilterDescriptions($filter_id);
    $fi = 0;
    $flen = count($filters);
    $g['filter'] = array();
    for($fi=0;$fi < $flen; $fi++){
      $fres =  array();
      $fres['filter_id'] = $filters[$fi]['filter_id'];
      $fres['sort_order'] = $filters[$fi]['sort_order'];
      $fres['filter_description'] =  $filters[$fi]['filter_description'];
      $old_data['filter'][] = $fres;
    }
    $data = $this->mergeData($old_data, $data);
    $fi = 0;
    $flen = count($data['filter']);
    for($fi=0;$fi < $flen; $fi++){
      if(!isset($data['filter'][$fi]['filter_id'])){
        $data['filter'][$fi]['filter_id'] = false;
      }
    }
    $this->model_catalog_filter->editFilter($filter_id, $data);
    $result = array();
    $result['status'] = 'ok';
    $result['filter_id'] = $filter_id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'filter');
  }

  // Delete a filtergroup based on filtergroup_id
  private function delete($filter_id){
    $test = $this->model_catalog_filter->getFilterGroup($filter_id);
    if(!$test){
      \Dge\Error::write($this,'E404');
    }else{
      unset($test);
    }
    // OC model delete
    $this->model_catalog_filter->deleteFilter($filter_id);
    $result['status'] = 'deleted';
    $result['filter_id'] = $filter_id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'filter');
  }

  protected function validateAndTransform($data){
    $res = true;
    if(isset($data['filter_group_description'])){
      list($err, $data['filter_group_description']) = $this->stranslateLocaleFromISO2($data['filter_group_description']);
    }
    if(isset($data['filter']) && \Dge\App::is_assoc($data['filter'])){
      $t = $data['filter'];
      $data['filter'] = array();
      $data['filter'][] = $t;
    }
    $fi = 0;
    $flen = count($data['filter']);
    $fres = array();
    for($fi=0;$fi < $flen;$fi++){
      $fltr = $data['filter'][$fi];
      list($err, $t)  = $this->stranslateLocaleFromISO2($fltr['filter_description']);
      $fltr['filter_description'] = $t;
      $fres[] = $fltr;
    }
    $data['filter'] = $fres;
    foreach ($data as $key => $value) {
      if( ( is_string($value) || is_numeric($value))
          // This fields should be arrays
          // (we can have multiple values)
          && (
               $key === 'filter'
          )
      ){
        // cast to array
        if($value !== '' || is_numeric($value) ){
          $data[$key] = array($value);
        }else{
          $data[$key] = array();
        }
        $value =  $data[$key];
      }
      $field = $key;
      if($key === 'filter_group_description'){

      }else if($key === 'filter'){

      }else if(is_array($value) && ! \Dge\App::is_assoc($value) ){
        // Value is an indexed array
        // check every single value
        $res = $this->validateArray($field, $value);
        if(!$res){
          return array($key, false);
        }
      }else{
        $res = $this->validate($field, $value);
      }
      if(!$res){
        return array($key, false);
      }
    }
    return array(false, $data);
  }

  protected function validate($type, $value){
    $res = false;
    if(!(is_string($value) || is_numeric($value))){
      # only check string or nummeric values
      return false;
    }

    switch ($type) {
      // Ints
      case 'filter_id':
      case 'sort_order':
      case 'language_id':
      case 'filter_group_id':
        $res = ((int)$value.'' == $value);
        break;
      case 'mf_tooltip':
        $res = true;
        break;
      case 'name':
        $res = (strlen($value.'') > 0 && strlen($value.'') < 101);
        break;
      case 'created':
      case 'date_added':
      case 'date_modified':
      case 'date_start':
      case 'date_end':
        // yyyy-mm-dd
        $pattern = '/^\d{4}-\d{2}-\d{2}$/';
        $test = preg_match ( $pattern , $value );
        if($test === 1){
          $res = true;
        }else{
          $res = false;
        }
        break;
      default:
        # code...
        break;
    }
    return $res;
  }

  protected function validateArray($field, $arr){
    $res = true;
    for($i = 0; $i < count($arr); $i++){
      $res = $this->validate($field, $arr[$i]);
      if(!$res){
        return false;
      }
    }
    return true;
  }
}
