<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;

class customergroupController extends \Dge\Controller {

  function __construct($params, $ct, $reg) {
    parent::__construct($ct, $reg);
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
      case 'GET':
      case 'POST':
      case 'PUT':
        if($method === 'PUT'){
          $method = 'POST';
        }
        $task = 'index';
        if(count($params) === 2 && $params[0] === 'delete'){
          $method = strtoupper(array_shift($params));
        }
        break;
      case 'DELETE':
        $task = 'index';
        break;
      default:
        $task = '';
        break;
    }
    $this->handleTask($task, $params, $method);
  }

  private function handleTask($task, $params, $method = 'GET'){
    switch ($task.'_'.$method) {
      case 'index_GET':
        $this->load->model('customer/customer_group');
        if($method === 'GET'){
          $id = $this->getIdParam($params);
          if($id){
            $data = $this->model_customer_customer_group->getCustomerGroup($id);
            if(!$data){
              \Dge\Error::write($this,'E404');
              die;
            }
            unset($data['language_id']);
            unset($data['name']);
            unset($data['description']);
            $data = array_merge($data,
              array('customer_group_description' => $this->model_customer_customer_group->getCustomerGroupDescriptions($id))
            );
            $data['customer_group_description'] =$this->stranslateLocaleFromId(
              $data['customer_group_description']
            );
            $this->writeHeaders();
            $this->write($data, 'customergroup');
            die;
          }

          $filter = array();
          isset($_REQUEST['start']) ? $filter['start'] = (int)$_REQUEST['start'] : $filter['start'] = 0;
          isset($_REQUEST['limit']) ? $filter['limit'] = (int)$_REQUEST['limit'] : $filter['limit'] = 20;
          $data = $this->model_customer_customer_group->getCustomerGroups($filter);
          $i = 0;
          $len = count($data);
          for($i = 0; $i < $len; $i++){
            if(array_key_exists('customer_group_description', $data[$i])){
              $data[$i]['customer_group_description'] =$this->stranslateLocaleFromId(
                $data[$i]['customer_group_description']
              );
            }
          }
          $this->writeHeaders();
          $this->write($data, 'customergroup');
        }
        break;
      case 'index_POST':
      case 'index_PUT':
        $id = $this->getIdParam($params);
        $data = $this->getPostData();
        // We need POST data
        if(!$data){
          \Dge\Error::write($this,'E400', 'Posted data is not readable');
        }
        $this->upsert($id,$data);
        break;
      case 'index_DELETE':
        $id = $this->getIdParam($params);
        $this->delete($id);
        break;
      default:
        \Dge\Error::write($this,'E501', 'Not Implemented');
        break;
    }
  }


  private function upsert($id, $data){
    $status = '200 Ok';
    $new = false;
    unset($data['customer_discount_matrix']);
    list($field_error, $data) = $this->validateAndTransform($data);
    if($field_error){
      \Dge\Error::write($this,'E001',$field_error);
      die;
    }
    $this->load->model('customer/customer_group');
    if(!$id){
      // Set defaults
      if(!isset($data['approval'])){
        $data['approval'] = 1;
      }
      if(!isset($data['company_id_display'])){
        $data['company_id_display'] = 1;
      }
      if(!isset($data['company_id_required'])){
        $data['company_id_required'] = 0;
      }
      if(!isset($data['tax_id_display'])){
        $data['tax_id_display'] = 1;
      }
      if(!isset($data['tax_id_required'])){
        $data['tax_id_required'] = 0;
      }
      if(!isset($data['sort_order'])){
        $data['sort_order'] = 0;
      }
      if(!isset($data['customer_group_description'])){
        \Dge\Error::write($this,'E002','customer_group_description');
        die;
      }
      // Make sure every language has a 'name'
      // only needed for the first time
      $pdesc = $this->stranslateLocaleFromId($data['customer_group_description']);
      if(! isset($pdesc['nl']) || ! isset($pdesc['nl']['name'])){
        \Dge\Error::write($this,'E002','customer_group_description - nl - name');
      }
      if(!isset($pdesc['nl']['description'])){
                $pdesc['nl']['description'] = '';
      }
      $data['customer_group_description'] = $this->CopyDescriptions($pdesc['nl']);
      // INSERT
      $g = \Dge\App::loadModel('customergroup');
      $id = $g->addCustomerGroup($data);
      $result = array();
      $result['status'] = 'ok';
      $result['customer_group_id'] = $id;
      $this->setResponseStatus('200 Ok');
      $this->writeHeaders();
      $this->write($result, 'customergroup');
    }

    $old_data = $this->model_customer_customer_group->getCustomerGroup($id);
    if(!$old_data){
      \Dge\Error::write($this,'E404');
            die;
    }
    unset($old_data['language_id']);
    unset($old_data['name']);
    unset($old_data['description']);
    $old_data = array_merge($old_data,
      array('customer_group_description' => $this->model_customer_customer_group->getCustomerGroupDescriptions($id))
    );
    $data = $this->mergeData($old_data, $data);
    $this->model_customer_customer_group->editCustomerGroup($id, $data);
    $result = array();
    $result['status'] = 'ok';
    $result['customer_group_id'] = $id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'customergroup');
    die;
  }

  // Delete a customer group
  private function delete($id){
    $this->load->model('customer/customer_group');
    $test = $this->model_customer_customer_group->getCustomerGroup($id);
    if(!$test || $id < 1){
      \Dge\Error::write($this,'E404');
    }else{
      unset($test);
    }
    // Delete
    $this->db->query("DELETE FROM " . DB_PREFIX . "tax_rate_to_customer_group WHERE customer_group_id = '" . (int)$id . "'");
    // OC model delete
    $this->model_customer_customer_group->deleteCustomerGroup($id);
    $result['status'] = 'deleted';
    $result['customer_group_id'] = $id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'customergroup');
  }

  // return TRUE on success
  protected function validate($type, $value){
    $res = false;
    if(!(is_string($value) || is_numeric($value))){
      # only check string or nummeric values
      return false;
    }

    switch ($type) {
      // Ints
      case 'customer_group_id':
      case 'sort_order':
      case 'language_id':
        $res = ( is_numeric($value)
                  && is_integer((int)$value)
                  && (int)$value.'' == $value
                );
        break;
      // tiny ints
      case 'company_id_display':
      case 'company_id_required':
      case 'tax_id_display':
      case 'tax_id_required':
      case 'approval':
        $res = (
          is_numeric($value)
          && is_integer((int)$value)
          && ( (int)$value == 0 || (int)$value == 1) );
        break;
      case 'name':
        $res = (strlen($value.'') > 2 && strlen($value.'') < 101);
        break;
      case 'description':
        $res = (is_string($value) || is_numeric($value));
        break;
      case 'created':
      case 'date_added':
      case 'date_modified':
      case 'date_start':
      case 'date_end':
        // yyyy-mm-dd
        $pattern = '/^\d{4}-\d{2}-\d{2}$/';
        $test = preg_match ( $pattern , $value );
        if($test === 1){
          $res = true;
        }else{
          $res = false;
        }
        break;
      default:
        # code...
        break;
    }
    return $res;
  }

  protected function validateArray($field, $arr){
    $res = true;
    for($i = 0; $i < count($arr); $i++){
      $res = $this->validate($field, $arr[$i]);
      if(!$res){
        return false;
      }
    }
    return true;
  }

  // Validate on all values we receive from a POST
  // convert when needed
  protected function validateAndTransform($data){
    $res = true;
    // Cleanup data which we should NOT recieve / overwrite
    foreach ($data as $key => $value) {
      if( \Dge\App::is_assoc($value) && $key === 'customer_discount_matrix'){
        // cast to array
        $data[$key] = array($value);
        $value =  $data[$key];
      }
      $field = $key;
      if($key === 'customer_group_description'){
          list($err, $new_ar) = $this->stranslateLocaleFromISO2($value);
          if($err){
            return array( 'customer_group_description : '.$err, false);
          }else{
            $data[$key] = $new_ar;
            $res = true;
          }
      }else if(is_array($value) && ! \Dge\App::is_assoc($value) ){
        $res = $this->validateArray($field, $value);
        if(!$res){
          return array($key, false);
        }
      }else{
          $res = $this->validate($field, $value);
      }
        if(!$res){
          return array($key, false);
        }
      }
      return array(false, $data);
  }
}
