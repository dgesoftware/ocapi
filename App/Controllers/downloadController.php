<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;

class downloadController extends \Dge\Controller {

  function __construct($params, $ct, $reg) {
    parent::__construct($ct, $reg);
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
      case 'GET':
      case 'POST':
      case 'PUT':
        if($method === 'PUT'){
          $method = 'POST';
        }
        $task = 'index';
        if(count($params) === 1 && $params[0] === 'statuslist'){
          $task = 'statuslist';
        }else if(count($params) === 2 && $params[0] === 'delete'){
          $method = strtoupper(array_shift($params));
        }else if(count($params) === 2 && $params[0] === 'status'){
          array_shift($params);
          $task = 'status';
        }
        break;
      case 'DELETE':
        $task = 'index';
        break;
      default:
        $task = '';
        break;
    }
    $this->handleTask($task, $params, $method);
  }

  private function handleTask($task, $params, $method = 'GET'){
    switch ($task.'_' . $method) {
      case 'index_POST':
      case 'index_PUT':
        // Status change for an order
        $id = $this->getIdParam($params);
	$data = $this->getPostData();
        if(!$data){
          \Dge\Error::write($this,'E400', 'Posted data is not readable');
        }
        if(!$id || $id < 1){
          \Dge\Error::write($this,'E400', 'Missing id');
        }
	$download = \Dge\App::loadModel('download');

	$res = $download->addDownloads($id,$data);
	$this->safeDocuments($id,$data);
        break;
      default:
        \Dge\Error::write($this,'E501', 'Not Implemented');
	break;
    }
  }

  private function safeDocuments($id,$data){
    $directory = DIR_IMAGE . 'dge_document/';		  
    if (!is_dir($directory)){
      mkdir($directory);
    }	
    foreach($data['document'] as $value){
        $data = base64_decode($value['data']);
        if (substr($data,0,5)=='%PDF-'){
            file_put_contents($directory . $value['sHash'],$data);		
	}	    
    }	    

  }	  
}
