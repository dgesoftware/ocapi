<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;


class manufacturerController extends \Dge\Controller {

  function __construct($params, $ct, $reg) {
    parent::__construct($ct, $reg);
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
      case 'GET':
      case 'POST':
      case 'PUT':
        if($method === 'PUT'){
          $method = 'POST';
        }
        $task = 'index';
        if(count($params) === 2 && $params[0] === 'delete'){
          $method = strtoupper(array_shift($params));
        }
        break;
      case 'DELETE':
        $task = 'index';
        break;
      default:
        $task = '';
        break;
    }
    $this->handleTask($task, $params, $method);
  }

  private function handleTask($task, $params, $method = 'GET'){
    switch ($task.'_'.$method) {
      case 'index_GET':
        $this->load->model('catalog/manufacturer');
        if($method === 'GET'){
          $manufacturer_id = $this->getIdParam($params);
          if($manufacturer_id){
            $this->get($manufacturer_id);
            return;
          }else{
            $this->index();
          }
        }
        break;
      case 'index_POST':
      case 'index_PUT':
        $this->load->model('catalog/manufacturer');
        $manufacturer_id = $this->getIdParam($params);
        $data = $this->getPostData();
        if(!$data){
          \Dge\Error::write($this,'E400', 'Posted data is not readable');
        }
        $this->upsert($manufacturer_id,$data);
        break;
      case 'index_DELETE':
        $manufacturer_id = $this->getIdParam($params);
        $this->load->model('catalog/manufacturer');
        $this->delete($manufacturer_id);
        break;
      default:
        \Dge\Error::write($this,'E501', 'Not Implemented');
        break;
    }
  }

  private function index(){
    $filter = array();
    isset($_REQUEST['start']) ? $filter['start'] = (int)$_REQUEST['start'] : $filter['start'] = 0;
    isset($_REQUEST['limit']) ? $filter['limit'] = (int)$_REQUEST['limit'] : $filter['limit'] = 20;
    $data = $this->model_catalog_manufacturer->getManufacturers($filter);
    $i = 0;
    $len = count($data);
    if($len === 0){
      // Not found / no result
      $this->setResponseStatus('404 Not Found');
    }
    $this->writeHeaders();
    $this->write($data, 'manufacturer');
  }

  private function get($manufacturer_id){
    $data = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
    if(!$data){
      $this->setResponseStatus('404 Not Found');
      $this->writeHeaders();
      $this->write(array(), 'manufacturer');
      die;
    }
    $this->writeHeaders();
    $this->write($data, 'manufacturer');
    die;
  }


  // if $category_id == false
  // we do an INSERT
  // ELSE an update
  private function upsert($manufacturer_id, $data){
    $status = '200 Ok';
    $new = false;
    list($field_error, $data) = $this->validateAndTransform($data);
    if($field_error){
      \Dge\Error::write($this,'E001',$field_error);
    }
    if(!$manufacturer_id){
      if(!isset($data['name']) || !is_string($data['name']) || strlen($data['name']) < 1){
        \Dge\Error::write($this,'E001','name');
      }
      if(!isset($data['sort_order'])){
        $data['sort_order'] = 0;
      }
      $manufacturer_id = $this->model_catalog_manufacturer->addManufacturer($data);
      if(!$manufacturer_id){
        \Dge\Error::write($this,'E500','Insert manufacturer');
      }
    }
    $old_data = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
    if(!$old_data){
      \Dge\Error::write($this,'E404');
      die;
    }
    $data = $this->mergeData($old_data, $data);
    if(isset($data['image_b64'])){
      // We have MAIN base64 image data
      // We 'r gonna use this to overwrite the initial image
      $img = $this->safeImage($manufacturer_id, $data['image_b64']);
      if($img && count($img) == 1){
        $data['image'] = $img['image'];
      }
      unset($data['image_b64']);
    }
    $this->model_catalog_manufacturer->editManufacturer($manufacturer_id, $data);
    $result = array();
    $result['status'] = 'ok';
    $result['manufacturer_id'] = $manufacturer_id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'manufacturer');
  }

  // Delete a catagory based on id
  private function delete($manufacturer_id){
    $test = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
    if(!$test){
      \Dge\Error::write($this,'E404');
    }else{
      unset($test);
    }
    // OC model delete
    $this->model_catalog_manufacturer->deleteManufacturer($manufacturer_id);
    $this->deleteImages($manufacturer_id);
    $result['status'] = 'deleted';
    $result['manufacturer_id'] = $manufacturer_id;
    $this->setResponseStatus('200 Ok');
    $this->writeHeaders();
    $this->write($result, 'manufacturer');
  }


  // Convert an array containing base64 image data
  // to an image and return an array of objects
  // ready for OC to insert
  private function safeImage($manufacturer_id, $arr){
    $main_image = false;
    if(is_string($arr)){
      // The filename is something like: data/c/[category_id]_0.jpg
      $main_image = true;
      $arr = array($arr);
    }else{
      return false;
    }
    $bina = array();
    $sort_order = 0;
    // Take the first 2 chars form the id to create a dirname
    $directory = DIR_IMAGE . 'catalog/m';
    if(!is_dir($directory)){
      mkdir($directory, 0777);
    }
    $subdir = substr($manufacturer_id.'', 0, 2);
    $directory = $directory.'/'.$subdir;
    if(!is_dir($directory)){
      mkdir($directory, 0777);
    }
    require_once(DGE_APP_PATH.'/Helpers/Binary.php');
    for($i=0;$i<count($arr);$i++){
      list($ct , $data) = \Dge\Binary::b64ToBin($arr[$i]);
      if($ct === 'image/jpeg'){
        $ext = '.jpg';
      }else{
        $ext = '.png';
      }
      $filename = $manufacturer_id.'_'.$ext;
      // Not a valid image?
      // Refuse the rest as well
      if($ct && $data){
        $image = 'catalog/m/'.$subdir.'/'.$filename;
         if($ct == 'image/gd'){
          // This is a GD image (converted BMP)
          // Store it as PNG
          imagepng($data, $directory.'/'.$filename, 0);
        }else{
          // Just write, leave it untouched
          $fp = fopen($directory.'/'.$filename, 'w');
          fwrite($fp, $data);
          fclose($fp);
        }
        $bina = array('image' => $image);
        $sort_order ++;
      }
    }
    return $bina;
  }

  private function deleteImages($manufacturer_id){
    $directory = DIR_IMAGE . 'catalog/m';
    $subdir = substr($manufacturer_id.'', 0, 2);
    $directory = $directory.'/'.$subdir;
    if(!is_dir($directory)){
      return true;
    }
    $files = glob($directory. '/'.$manufacturer_id.'_*');
    if ($files) {
      foreach ($files as $file) {
        unlink ( $file );
      }
    }
    return true;
  }

  protected function validateAndTransform($data){
    $res = true;
    // Cleanup data which we should NOT recieve / overwrite
    // These values are for internal use only
    if(isset($data['image'])){
      unset($data['image']);
    }
    foreach ($data as $key => $value) {
      if( ( is_string($value) || is_numeric($value))
          // This fields should be arrays
          // (we can have multiple values)
          && (
               $key === 'manufacturer_store'
          )
      ){
        // cast to array
        $data[$key] = array($value);
        $value =  $data[$key];
      }
    $field = $key;
    if ($field === 'manufacturer_store') $field = 'store_id';
    if(is_array($value) && ! \Dge\App::is_assoc($value) ){
      // Value is an indexed array
      // check every single value
      $res = $this->validateArray($field, $value);
      if(!$res){
        return array($key, false);
      }
    }else{
      $res = $this->validate($field, $value);
    }
      if(!$res){
        return array($key, false);
      }
    }
    return array(false, $data);
  }

 // true on success
  protected function validate($type, $value){
    $res = false;
    if(!(is_string($value) || is_numeric($value))){
      # only check string or nummeric values
      return false;
    }

    switch ($type) {
      // Ints
      case 'manufacturer_id':
      case 'sort_order':
      case 'language_id':
      case 'store_id':
      case 'manufacturer_store':
        $res = ((int)$value.'' == $value);
        break;
      // tiny ints
      case 'image':
      case 'keyword':
        $res = (strlen($value.'') < 256);
        break;
      case 'name':
        $res = (strlen($value.'') > 0 && strlen($value.'') < 101);
        break;
      case 'image_b64':
        $res = (is_string($value) || is_numeric($value));
        break;
      case 'created':
      case 'date_added':
      case 'date_modified':
      case 'date_start':
      case 'date_end':
        // yyyy-mm-dd
        $pattern = '/^\d{4}-\d{2}-\d{2}$/';
        $test = preg_match ( $pattern , $value );
        if($test === 1){
          $res = true;
        }else{
          $res = false;
        }
        break;
      default:
        break;
    }
    return $res;
  }

  protected function validateArray($field, $arr){
    $res = true;
    for($i = 0; $i < count($arr); $i++){
      $res = $this->validate($field, $arr[$i]);
      if(!$res){
        return false;
      }
    }
    return true;
  }
}
