<?php

namespace Helper;
/*
 * ### ACL Client
 *
 * TIP: use as singleton e.g.
 *
 *    \Helper\AclClient::getService('as..','234234','https://w...','admin')
 *
 * Baseclass for checking a token / service UID against the ACL service
 * and cache the result for 24 hours
 *
 * Requirements:
 * * php-crypt
 * * php.ini  allow_url_fopen=true
 *   http://www.php.net/manual/en/filesystem.configuration.php#ini.allow-url-fopen
 *
 */

class AclClient {

  # ----------------------------------------------------------------------------
  # ## CONSTANTS
  # ----------------------------------------------------------------------------
  # seconds before a time-out occurs
  const REQ_TIMEOUT = 4;

  # CACHE EXPIRE TIME
  # we do not want an acl request
  # on every service request
  # EXPIRE every 24 hours
  const MAX_CACHE_TIME = 86400; // 24 hour

  # user agent string, for acces log statistics?
  const USER_AGENT = 'PHP Acl Client 1.0'; //

  # secret for cached content
  const SECRET = '!345778__dsfgAb#';

  # default ACL cache location
  public static $cache_dir = '/tmp/cacl';


  # ----------------------------------------------------------------------------
  # ## PUBLIC API
  # ----------------------------------------------------------------------------

  # ### getService
  #
  # get a service based on endpoint and role
  # when there is a succesfull acl request
  # the result is cached for 24 hours
  # so there will be only acl requests every 24 hours
  #
  # ### Params:
  #
  # * $xauth, String, acl x-auth-token
  # * $xservice, String, acl service UID
  # * $endpoint, String url to API
  # * $role, String 'user', 'author', 'admin'
  #
  # ### Return:
  #
  #   associated array e.g. = JSON representation
  #
  #   { service:
  #     { service: 'Test Service',
  #       role: 'user',
  #       endpoint: 'https://test.dge.local:85',
  #       UID: 'ea46810c78469cd4dcbc1d0eb18320e7',
  #       docs: 'https://test.dge.local:85/docs',
  #       metadata: null,
  #       global: false
  #     }
  #   }

  static function getService($xauth, $xservice, $endpoint, $role, $acl_url = false){

      if($xauth == '' || $xservice == '' || $endpoint == '' || $role == ''){
        return false;
      }
      // ---- En/decrypt keys ------------------------
      $secret = hash('sha256',self::SECRET,TRUE);
      // ---------------------------------------------

      if(!is_dir(self::$cache_dir)){
        mkdir(self::$cache_dir);
      }

      $cacheData = false;
      $cacheFile = self::$cache_dir.'/'.md5($xauth.$xservice.$endpoint.$role);
      $cacheData = self::getCacheData($cacheFile, $secret);
      if($cacheData){
        // Is this outdated?
        $now = time();
        $ft = filemtime ($cacheFile);
        if( ($now - $ft) >= (self::MAX_CACHE_TIME) ){
          // nothing yet
          // we have expired cache data
          //
        }else{
          // This is valid data
          // we do not GC
          return $cacheData;
        }

      }
      // Here we have old cacheData or a new request
      // Create request context
      $opts = array(
      'http'=>array(
        'protocol_version'=>1.1,
        'user_agent' => self::USER_AGENT,
        'header'=>"x-auth-token: {$xauth}\r\n" .
                  "Accept: application/json\r\n" .
                  "Connection: close\r\n",
        'timeout'=> self::REQ_TIMEOUT
      ));
      $context = stream_context_create($opts);

      if(!$acl_url){
        throw new \Exception("Endpoint for authentication not set");
      }else{
        $acl_url = $acl_url.'endpoint/'.$xservice;
      }

      $data = json_decode(
        @file_get_contents($acl_url, false, $context)
      , true);
      if ( isset($http_response_header) ){
        // We have response
        // do a garbage collect
        // (do not garbage collect when there is no connection to the ACL)
        self::garbageCollectCache();
        if($http_response_header[0] === 'HTTP/1.1 200 OK' && isset($data) ){
          if(
            isset($data['service'])
            && isset($data['service']['endpoint'])
            && isset($data['service']['role'])
            && $data['service']['endpoint'] === $endpoint
            && $data['service']['role'] === $role
            ){
            self::setCacheData($cacheFile, $data, $secret);
            return $data;
          }
        }else{
          if($cacheData){
            // we have cache data but an invalid response
            // remove the file
            @unlink($cacheFile);
          }
          return false; // no valid response
        }

      }else{
        // We have no response header (=== no connection)
        // we where not able to revalidate the cache
        // so we returning 'old' data
        // and do nothing (acl service is down?)
        return $cacheData;
      }

  }

  # ----------------------------------------------------------------------------
  # PROTECTED API
  # ----------------------------------------------------------------------------

  ### encrypt($input, $secret)
  # Encrypt a string
  static protected function encrypt($input, $secret) {
	  $method ="AES-128-CBC";
    $ivlen = openssl_cipher_iv_length($method);
    $iv = openssl_random_pseudo_bytes($ivlen);
    	  
	  return base64_encode(openssl_encrypt($input, $method, $secret,0,$iv));
  }

  # ### decrypt($input, $secret)
  # Decrypt a string
  static protected function decrypt($input, $secret) {
    $method ="AES-128-CBC";
    return trim(openssl_decrypt(base64_decode($input), $method, $secret));
  }



  # ### setCacheData($fp, $data, $secret)
  # Store data in a cache file
  #
  static protected function setCacheData($file, $data, $secret){
    $data = self::encrypt(json_encode($data),$secret);
    file_put_contents($file, $data);
    return;
  }

  # ### getCacheData()
  # Get the cached data
  #
  static protected function getCacheData($file, $secret){
     if( file_exists ($file)){
      try {
        $data = json_decode(self::decrypt(file_get_contents($file),$secret), true);
      } catch (\Exception $e) {
        // When data in invalid, the file wil be deleted
        @unlink($file);
        $data = false;
      }
      return $data;
     }else{
      return false;
     }
  }
  # ## garbageCollectCache
  #
  # Delete every file from cache equal or older then MAX_CACHE_TIME
  static protected function garbageCollectCache(){
    $now = time();
    if ($handle = opendir(self::$cache_dir)) {
      while (false !== ($entry = readdir($handle))) {
          if ($entry != "." && $entry != "..") {
            $fp = self::$cache_dir.'/'.$entry;
            $ft = filemtime ($fp);
            // Build in extra time
            // to recover from a crached acl server
            // one week cache
            if( ($now - $ft) >= (self::MAX_CACHE_TIME * 7) ){
              @unlink($fp);
            }
          }
      }
      closedir($handle);
    }
  }

}
