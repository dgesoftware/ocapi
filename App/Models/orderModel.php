<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;

class orderModel extends \Dge\Model {

  private $languages = false;

  function __construct($reg) {
    parent::__construct($reg);
  }
  function __destruct() {
    parent::__destruct();
  }

  // Get a list of orders with optional filters in place
  function getCollection($data){
    $res = array();
    // $this->load->model('sale/order');
    // $data = $this->model_sale_order->getOrders($filter);
    // Code is copied 1: 1 from sale/order->getOrders
    // #OC-36 Adding order payment string/code
    // <payment_method>Onder rembours</payment_method>
    // <payment_code>cod</payment_code>
    $sql = "SELECT o.order_id, o.store_id, o.payment_method, o.payment_code, CONCAT(o.firstname, ' ', o.lastname) AS customer, o.order_status_id, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified FROM `" . DB_PREFIX . "order` o";

    $settings = \Dge\App::getSettings();
    $pending = (int)$settings['order_status']['pending'];
    $paid = (int)$settings['order_status']['paid'];
    $sql .= " WHERE o.order_status_id = '" . (int)$pending . "' OR o.order_status_id = '" . (int)$paid . "'";
    $sort_data = array(
      'o.order_id',
      'customer',
      'status',
      'o.date_added',
      'o.date_modified',
      'o.total'
    );
    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else {
      $sql .= " ORDER BY o.order_id";
    }
    if (isset($data['order']) && ($data['order'] == 'DESC')) {
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }
    if (isset($data['start']) || isset($data['limit'])) {
      if ($data['start'] < 0) {
        $data['start'] = 0;
      }
      if ($data['limit'] < 1) {
        $data['limit'] = 20;
      }
      $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }

    $query = $this->db->query($sql);
    $res = $query->rows;
    for($i=0;$i<count($res);$i++){
      // translate order_status_id to DGE status
      if((int)$res[$i]['order_status_id'] === $paid){
        $res[$i]['status'] = 'paid';
      }else{
        $res[$i]['status'] = 'pending';
      }
    }
    return $res;
  }

  // get the details
  function get($id){
    $this->load->model('sale/order');
    $data = $this->model_sale_order->getOrder($id);
    if(!$data) {
      return false;
    }
    $data['customField']= $this->getCustomfields($id);
    $data['order_products'] = $this->model_sale_order->getOrderProducts($id);
    for($i=0;$i<count($data['order_products']);$i++){
      $data['order_products'][$i]['order_options'] = $this->model_sale_order->getOrderOptions($id, $data['order_products'][$i]['order_product_id']);
      for($i2=0;$i2<count($data['order_products'][$i]['order_options']);$i2++){
        $data['order_products'][$i]['order_options'][$i2]['order_option'] = $this->getOrderOption($id, $data['order_products'][$i]['order_options'][$i2]['order_option_id']);
      }
    }
    $data['order_vouchers'] = $this->model_sale_order->getOrderVouchers($id);
    $data['order_totals'] = $this->model_sale_order->getOrderTotals($id);
    $data['order_histories'] = $this->model_sale_order->getOrderHistories($id,0,100);
    return $data;
  }

  function add($data){
    // Copied FROM admin/model/cutomer.php
    $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', newsletter = '" . (int)$data['newsletter'] . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', status = '" . (int)$data['status'] . "', date_added = NOW()");
    $customer_id = $this->db->getLastId();
    if (isset($data['address'])) {
      foreach ($data['address'] as $address) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($address['firstname']) . "', lastname = '" . $this->db->escape($address['lastname']) . "', company = '" . $this->db->escape($address['company']) . "', company_id = '" . $this->db->escape($address['company_id']) . "', tax_id = '" . $this->db->escape($address['tax_id']) . "', address_1 = '" . $this->db->escape($address['address_1']) . "', address_2 = '" . $this->db->escape($address['address_2']) . "', city = '" . $this->db->escape($address['city']) . "', postcode = '" . $this->db->escape($address['postcode']) . "', country_id = '" . (int)$address['country_id'] . "', zone_id = '" . (int)$address['zone_id'] . "'");

        if (isset($address['default'])) {
          $address_id = $this->db->getLastId();

          $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . $address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
        }
      }
    }

    if(isset($data['approved'])){
      $this->load->model('sale/customer');
      $this->model_sale_customer->approve($customer_id);
    }
    return $customer_id;
  }

  // return TRUE on success
  function validate($type, $value){
    $res = false;
    switch ($type) {
      // Ints
      case 'order_id':
        $res = ( is_numeric($value)
                  && is_integer((int)$value)
                  && (int)$value.'' == $value
                );
        break;
      // tiny ints
      case 'notify':
        $res = (
          is_numeric($value)
          && is_integer((int)$value)
          && ( (int)$value == 0 || (int)$value == 1) );
        break;
      case 'order_status':
      case 'comment':
        $res = (is_numeric($value) || is_string($value));
        break;
      case 'date_added':
      case 'date_modified':
        // yyyy-mm-dd
        $pattern = '/^\d{4}-\d{2}-\d{2}$/';
        $test = preg_match ( $pattern , $value );
        if($test === 1){
          $res = true;
        }else{
          $res = false;
        }
        break;
      default:
        # code...
        break;
    }
    return $res;
  }

  function validateArray($field, $arr){
    $res = true;
    for($i = 0; $i < count($arr); $i++){
      $res = $this->validate($field, $arr[$i]);
      if(!$res){
        return false;
      }
    }
    return true;
  }

  function validateAndTransform($data){
    $res = true;
    // Cleanup data which we should NOT recieve / overwrite
    foreach ($data as $key => $value) {
      $field = $key;
      if($key == 'email'){
        // email always LOWERCASE
        $data[$key] = strtolower($value);
        $value = strtolower($value);
      }

      if($key == 'address' ){
        if(\Dge\App::is_assoc($value)){
          $data[$key] = array($value);
        }
        $value = $data[$key];
        list($err, $addr) = $this->addresCheck($data[$key]);
        if($err){
          return array($err, false);
        }else{
          $data[$key] = $addr;
          $res = true;
        }
      }else if(is_array($value) && ! \Dge\App::is_assoc($value) ){
        // Value is an indexed array
        // check every single value
        $res = $this->validateArray($field, $value);
        if(!$res){
          return array($key, false);
        }
      }else{
        $res = $this->validate($field, $value);
      }
      if(!$res){
        return array($key, false);
      }
    }
    return array(false, $data);
  }


  // get a keyword and translate it to an status_id
  // see settings.json file
  public function getStatusId($indication){
    // Find the occurence of 'hoog' of 'laag'
    // in combination with 'btw'
    $settings = \Dge\App::getSettings();
    if(!$settings){
      return false;
    }
    $order_class_id = false;
    if(array_key_exists ($indication, $settings["order_status"])){
      $order_class_id = $settings["order_status"][$indication];
    }
    if($order_class_id === false){
      return false;
    }

    $this->load->model('localisation/order_status');
    $classes = $this->model_localisation_order_status->getOrderStatuses();
    for($i=0;$i<count($classes);$i++){
      if($order_class_id == $classes[$i]['order_status_id']){
        return $classes[$i]['order_status_id'];
      }
    }
    return false;
  }

  // get a keyword and translate it to an status_id
  // see settings.json file
  public function getStatus($indication){
    // Find the occurence of 'hoog' of 'laag'
    // in combination with 'btw'
    $settings = \Dge\App::getSettings();
    if(!$settings){
      return false;
    }
    $order_class_id = false;
    if(array_key_exists ($indication, $settings["order_status"])){
      $order_class_id = $settings["order_status"][$indication];
    }
    if($order_class_id === false){
      return false;
    }

    $this->load->model('localisation/order_status');
    $classes = $this->model_localisation_order_status->getOrderStatuses();
    for($i=0;$i<count($classes);$i++){
      if($order_class_id == $classes[$i]['order_status_id']){
        return $classes[$i];
      }
    }
    return false;
  }
 
  public function getCustomfields($order_id) {
    $query = $this->db->query("SELECT custom_field FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");
    $custom= $query->row['custom_field'];

    return $custom;
  }

  public function getOrderOption($order_id, $order_option_id) {
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_option_id = '" . (int)$order_option_id . "'");

    return $query->row;
  }

  // We have VALIDATED post data
  // here w 'r gonna merge the data
  // we only overwrite what is given
  // the data itself is allready sanitized
  public function mergeData($id, $data){
    $old = $this->getCustomer($id);
    if(!$old){
      return array('Customer not found:'.$id, false);
    }else{
      // W'r not going to merge this
      $new = array_replace_recursive($old, $data);
      return array(false, $new);
    }
  }

  public function getStatusList(){

    // Get default order_status
    $default = (int)$this->config->get('config_order_status_id');

    $sql = 'SELECT * FROM `'. DB_PREFIX .'order_status` ';
    $query = $this->db->query($sql);
    if(!$query->num_rows) {
      return false;
    }

    $data =  $query->rows;

    $res = array();

    for($i=0; $i<count($data);$i++){

      $lang_id = (int)$data[$i]['language_id'];
      $order_status_id = $data[$i]['order_status_id'];
      $name = $data[$i]['name'];


      if(!isset($res[$lang_id])){
        $res[$lang_id] = array();
        $res[$lang_id]['orderstatus'] = array();
      }

      $tmp = array();
      // $tmp['language_id'] = $lang_id;
      $tmp['order_status_id'] = $order_status_id;
      $tmp['name'] = $name;

      if($default === (int)$order_status_id){
        $tmp['default'] = 1;
      }


      $res[$lang_id]['orderstatus'][] = $tmp;

    }

    $res = $this->stranslateLocaleFromId($res);

    return $res;
  }

public function addOrderHistory($order_id, $data) {
    $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$data['order_status_id'] . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
    $_comment = isset($data['comment']) ? $this->db->escape(strip_tags($data['comment'])) : '';
    $this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$data['order_status_id'] . "', notify = '0', comment = '" .  $_comment . "', date_added = NOW()");
  }

}
