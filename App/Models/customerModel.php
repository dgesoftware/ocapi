<?php
/*

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;

class customerModel extends \Dge\Model {

  function __construct($reg) {
    parent::__construct($reg);
  }
  function __destruct() {
    parent::__destruct();
  }

  public function addCustomer($data) {
    if(!array_key_exists("custom_field",$data)){
          $data['custom_field'] = '';
    }
    $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(serialize($data['custom_field'])) . "', newsletter = '" . (int)$data['newsletter'] . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', status = '" . (int)$data['status'] . "', safe = '" . (int)$data['safe'] . "', date_added = NOW()");

    $customer_id = $this->db->getLastId();
    if (isset($data['address'])) {
      foreach ($data['address'] as $address) {
        if(!array_key_exists("custom_field",$address)){
          $address['custom_field'] = '';
        }
        $this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($address['firstname']) . "', lastname = '" . $this->db->escape($address['lastname']) . "', company = '" . $this->db->escape($address['company']) . "', address_1 = '" . $this->db->escape($address['address_1']) . "', address_2 = '" . $this->db->escape($address['address_2']) . "', city = '" . $this->db->escape($address['city']) . "', postcode = '" . $this->db->escape($address['postcode']) . "', country_id = '" . (int)$address['country_id'] . "', zone_id = '" . (int)$address['zone_id'] . "', custom_field = '" . $this->db->escape(serialize($address['custom_field'])) . "'");

        if (isset($address['default'])) {
          $address_id = $this->db->getLastId();
          $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
        }
      }
    }
    return  $customer_id;
  }

  public function setOnAccount($customer_id, $value){
    $sql = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."dge_onaccount` (  `customer_id` int(11) NOT NULL, `value` int(11) NOT NULL, PRIMARY KEY (`customer_id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
    $query = $this->db->query($sql);
    $sql = "DELETE FROM `".DB_PREFIX."dge_onaccount` WHERE `customer_id` = ".intval($customer_id).";";
    $query = $this->db->query($sql);
    $sql = "INSERT INTO `".DB_PREFIX."dge_onaccount` (`customer_id`,`value`) VALUES (".intval($customer_id).",".intval($value).")";
    $query = $this->db->query($sql);
  }
  public function getOnAccount($customer_id){
    $sql = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."dge_onaccount` (  `customer_id` int(11) NOT NULL, `value` int(11) NOT NULL, PRIMARY KEY (`customer_id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
    $query = $this->db->query($sql);
    $sql = "SELECT value  FROM `".DB_PREFIX."dge_onaccount` WHERE `customer_id` = ".intval($customer_id).";";
    $query = $this->db->query($sql);
		return $query->row;
  }

  public function getMasterAccount($customer_id){
    $sql = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."dge_subaccounts` ( `customer_id` int(11) NOT NULL, `subcustomer_id` int(11) NOT NULL,  `basket` tinyint NOT NULL DEFAULT '1', `prices` tinyint NOT NULL DEFAULT '1', PRIMARY KEY (`customer_id`,`subcustomer_id`)  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
    $query = $this->db->query($sql);
    $query = $this->db->query("SELECT customer_id from `" . DB_PREFIX  . "dge_subaccounts` WHERE subcustomer_id = '" . (int)$customer_id  . "'");
    $master = 0;
    if ($query->num_rows) {
       $master = $query->row['customer_id'];
    }
    return $master;
  }

}
