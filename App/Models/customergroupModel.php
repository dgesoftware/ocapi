<?php
/*
This file is part of OCAPI
Opencart HTTP(s) XML/JSON API

(source:)[https://bitbucket.org/dgesoftware/ocapi]
(wiki:)[https://bitbucket.org/dgesoftware/ocapi/wiki/Home]
(issues:)[https://bitbucket.org/dgesoftware/ocapi/issues]

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Dge;

class customergroupModel extends \Dge\Model {

  function __construct($reg) {
    parent::__construct($reg);
  }
    function __destruct() {
        parent::__destruct();
    }

    public function addCustomerGroup($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group SET approval = '" . (int)$data['approval'] . "', sort_order = '" . (int)$data['sort_order'] . "'");
        $customer_group_id = $this->db->getLastId();
        foreach ($data['customer_group_description'] as $language_id => $value) {
            if(strlen($value['name'])> 32){
              $value['name'] = 'dge_'.$customer_group_id;
            }
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description SET customer_group_id = '" . (int)$customer_group_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }
        // Add tax rates to GROUPS
        $t = $this->getTaxIds();
        for ($i=0; $i < count($t); $i++) {
            $tax_rate_id = $t[$i]['tax_rate_id'];
            $this->db->query("INSERT INTO " . DB_PREFIX . "tax_rate_to_customer_group SET tax_rate_id = '" . (int)$tax_rate_id . "', customer_group_id = '" . (int)$customer_group_id . "'");
        }
        return $customer_group_id ;
    }

    public function getTaxIds(){
       $sql = 'SELECT tax_rate_id FROM ' . DB_PREFIX . 'tax_rate';
       $res = $this->db->query($sql);
       return $res->rows;
    }
}
