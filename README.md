# OCAPI
---
This repository is no longer maintained. No changes will be made after 2023. Future integrations will only be allowed on the DGEAPI.
---

Copyright (C) 2014  Jorrit Duin, http://www.dgebv.nl
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


An HTTP(s) xml/json api on top of [opencart](http://www.opencart.com/) 3.0

* [Wiki](https://bitbucket.org/dgesoftware/ocapi/wiki/Home)
* [Install](https://bitbucket.org/dgesoftware/ocapi/wiki/Installation)
* [Issues](https://bitbucket.org/dgesoftware/ocapi/issues)
