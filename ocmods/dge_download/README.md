#INSTALL

Deze extensie werkt samen met de OCAPI. 

Zorg ervoor dat de 'dge_download' bestaat (Voer anders onderstaande SQL uit.);
CREATE TABLE IF NOT EXISTS `oc_dge_download` (
  `dge_download_id` int AUTO_INCREMENT PRIMARY KEY,
  `product_id` int NOT NULL,
  `label` varchar(100) NOT NULL,
  `link_text` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=UTF8MB4;
