#!/usr/bin/env node

const assert = require('assert');
const path = require('path');
const fs = require('fs');
const acl = require('../../lib/aclclient.js');
const Parker = require('parker-promise');
const startTime = new Date().getTime();

const request = require('../../lib/ocRequest.js');

var settings;
var authToken;
var serviceToken;

var group1;
var att1;

const color = {
  BLACK: '\u001b[30m',
  RED: '\u001b[31m',
  GREEN: '\u001b[32m',
  YELLOW: '\u001b[33m',
  BLUE: '\u001b[34m',
  MAGENTA: '\u001b[35m',
  CYAN: '\u001b[36m',
  WHITE: '\u001b[37m',
  RESET: '\u001b[0m'
};

try {
  let tst = fs.readFileSync(path.resolve(__dirname, '../../settings.json'));
  settings = JSON.parse(tst.toString());
} catch (e) {
  console.error(e);
  console.log('* Unable to open settings.json *');
  console.log('cp settings.json.example settings.json');
  process.exit(1);
}
try {
  fs.statSync(path.resolve(__dirname, '../../cache'));
} catch (e) {
  fs.mkdirSync(path.resolve(__dirname, '../../cache'));
}

acl.setCachePath(path.resolve(__dirname, '../../cache'));

const test = new Parker();

test.Do('Obtain tokens',
  (p) => {
    process.stdout.write('.');
    acl.getAccount(settings.username, settings.password, settings.aclUrl, function (error, account) {
      if (error) {
        console.error(error);
        p.fail();
        return;
      }
      let service = account.getService('opencart', 'admin', settings.endpoint);
      assert.equal(service.length, 1);
      serviceToken = service[0].UID;
      authToken = account.token;
      request.setEndpoint(settings.endpoint);
      request.setTokens(authToken, serviceToken);
      p.done();
    });
  }
).thenDo('Upsert an attribute group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attributegroup.json');
    // This is a complete group (with default values)
    var group = {
      sort_order: 0,
      attribute_group_description: {
        nl: {
          name: 'attribute_group'
        }
      }
    };
    var pst = request.toJson(group);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw res;
      }
      if (res.statusCode === 501) {
        console.log('Attribute groups not implemented');
        console.log('Stopping test');
        process.exit(1);
      }
      group1 = parseInt(res.body.attribute_group_id);
      assert.equal((group1 > 0), true);
      assert.equal(res.body.status, 'ok');
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('insert an attribute',
  (p) => {
    process.stdout.write('.');
    // attr_url
    var attr = {
      sort_order: 1,
      attribute_group_id: group1,
      attribute_description: {
        nl: {
          name: 'tester2'
        }
      }
    };
    var api = request.getApi('attribute');
    var pst = request.toJson(attr);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      att1 = parseInt(res.body.attribute_id);
      assert.equal((att1 > 0), true);
      p.done();
    });
  }
).thenDo('Get an attribute',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attribute/' + att1 + '.json');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(parseInt(res.body.attribute_id), att1);
      p.done();
    });
  }
).thenDo('update an attribute',
  (p) => {
    process.stdout.write('.');
    // attr_url
    var attr = {
      sort_order: 1,
      attribute_group_id: group1,
      attribute_description: {
        nl: {
          name: 'tester update'
        }
      }
    };
    var api = request.getApi('attribute/' + att1 + '.json');
    var pst = request.toJson(attr);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      var test = parseInt(res.body.attribute_id);
      assert.equal(att1, test);
      p.done();
    });
  }
).thenDo('Get the group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attributegroup/' + group1 + '.json');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.attribute_group_description.nl.name, 'attribute_group');
      assert.equal(res.body.sort_order, '0');
      p.done();
    });
  }
).thenDo('Delete an attribute',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attribute/' + att1 + '.json');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get an attribute',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attribute/' + att1 + '.json');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
).thenDo('Delete the group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attributegroup/' + group1 + '.json');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get the group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attributegroup/' + group1 + '.json');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
);
test.whenDone(() => {
  let endTime = new Date().getTime();
  console.log(color.GREEN + 'Succesfull finished in: ' + (endTime - startTime) + ' ms' + color.RESET);
  process.exit();
}).whenFail((e) => {
  process.stdout.write(color.RED);
  console.error(e);
  process.stdout.write(color.RESET);
  process.exit(1);
});
