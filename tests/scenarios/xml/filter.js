#!/usr/bin/env node

const assert = require('assert');
const path = require('path');
const fs = require('fs');
const acl = require('../../lib/aclclient.js');
const Parker = require('parker-promise');
const startTime = new Date().getTime();

const request = require('../../lib/ocRequest.js');

var settings;
var authToken;
var serviceToken;
var fgroup1;
var filterId;

const color = {
  BLACK: '\u001b[30m',
  RED: '\u001b[31m',
  GREEN: '\u001b[32m',
  YELLOW: '\u001b[33m',
  BLUE: '\u001b[34m',
  MAGENTA: '\u001b[35m',
  CYAN: '\u001b[36m',
  WHITE: '\u001b[37m',
  RESET: '\u001b[0m'
};

try {
  let tst = fs.readFileSync(path.resolve(__dirname, '../../settings.json'));
  settings = JSON.parse(tst.toString());
} catch (e) {
  console.error(e);
  console.log('* Unable to open settings.json *');
  console.log('cp settings.json.example settings.json');
  process.exit(1);
}
try {
  fs.statSync(path.resolve(__dirname, '../../cache'));
} catch (e) {
  fs.mkdirSync(path.resolve(__dirname, '../../cache'));
}

acl.setCachePath(path.resolve(__dirname, '../../cache'));

const test = new Parker();

test.Do('Obtain tokens',
  (p) => {
    process.stdout.write('.');
    acl.getAccount(settings.username, settings.password, settings.aclUrl, function (error, account) {
      if (error) {
        console.error(error);
        p.fail();
        return;
      }
      let service = account.getService('opencart', 'admin', settings.endpoint);
      assert.equal(service.length, 1);
      serviceToken = service[0].UID;
      authToken = account.token;
      request.setEndpoint(settings.endpoint);
      request.setTokens(authToken, serviceToken);
      p.done();
    });
  }
).thenDo('get the index of filter groups',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('filtergroup.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      if (res.statusCode === 501) {
        console.log('filter groups not implemented');
        console.log('Stopping test');
        process.exit(1);
      }
      p.done();
    });
  }
).thenDo('Add a filter group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('filtergroup.xml');
    var filter = {
      sort_order: 0,
      filter_group_description: {
        nl: { name: 'Filter Group name' }
      },
      filter: [
        { sort_order: 1, filter_description: { nl: { name: 'Filter name' } } }
      ]
    };
    var pst = request.toXml(filter);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw res;
      }
      assert.equal(res.statusCode, 200);
      fgroup1 = parseInt(res.body.filter.filter_id);
      assert.equal((fgroup1 > 0), true);
      p.done();
    });
  }
).thenDo('Get the filter group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('filtergroup/' + fgroup1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw res;
      }
      filterId = res.body.filter.filter.filter_id;
      assert.equal(filterId > 0, true);
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.filter.filter.filter_description.nl.name, 'Filter name');
      p.done();
    });
  }
).thenDo('Update a filter',
  // To obtain filter_id's
  // we need to add a filter_id with the filter
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('filtergroup/' + fgroup1 + '.xml');
    var filter = {
      sort_order: 0,
      filter_group_description: {
        nl: { name: 'Filter Group name' }
      },
      filter: [
        { filter_id: filterId, sort_order: 1, filter_description: { nl: { name: 'Filter name' } } },
        { sort_order: 2, filter_description: { nl: { name: 'Filter name 2' } } }
      ]
    };
    var pst = request.toXml(filter);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw res;
      }
      assert.equal(res.statusCode, 200);
      fgroup1 = parseInt(res.body.filter.filter_id);
      assert.equal((fgroup1 > 0), true);
      p.done();
    });
  }
).thenDo('Get the filter group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('filtergroup/' + fgroup1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw res;
      }
      assert.equal(res.statusCode, 200);
      let i = 0, len = res.body.filter.filter.length;
      let found = false;
      for (;i < len; i++) {
        if (res.body.filter.filter[i].filter_id === filterId) {
          found = true;
        }
      }
      assert.equal(found, true);
      assert.equal(res.body.filter.filter.length, 2);
      p.done();
    });
  }
);

test.thenDo('Delete a filter group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('filtergroup/' + fgroup1 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw res;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get a 404',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('filtergroup/' + fgroup1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw res;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
);

test.whenDone(() => {
  let endTime = new Date().getTime();
  console.log(color.GREEN + 'Succesfull finished in: ' + (endTime - startTime) + ' ms' + color.RESET);
  process.exit();
}).whenFail((e) => {
  process.stdout.write(color.RED);
  console.error(e);
  process.stdout.write(color.RESET);
  process.exit(1);
});
