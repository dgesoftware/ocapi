#!/usr/bin/env node

const assert = require('assert');
const path = require('path');
const fs = require('fs');
const acl = require('../../lib/aclclient.js');
const Parker = require('parker-promise');
const startTime = new Date().getTime();

const request = require('../../lib/ocRequest.js');

var settings;
var authToken;
var serviceToken;
var orderId;
var orderStatus;
var orderCompleteStatus;
var orderPendingStatus;
var orderPaidStatus;

const color = {
  BLACK: '\u001b[30m',
  RED: '\u001b[31m',
  GREEN: '\u001b[32m',
  YELLOW: '\u001b[33m',
  BLUE: '\u001b[34m',
  MAGENTA: '\u001b[35m',
  CYAN: '\u001b[36m',
  WHITE: '\u001b[37m',
  RESET: '\u001b[0m'
};

try {
  let tst = fs.readFileSync(path.resolve(__dirname, '../../settings.json'));
  settings = JSON.parse(tst.toString());
} catch (e) {
  console.error(e);
  console.log('* Unable to open settings.json *');
  console.log('cp settings.json.example settings.json');
  process.exit(1);
}
try {
  fs.statSync(path.resolve(__dirname, '../../cache'));
} catch (e) {
  fs.mkdirSync(path.resolve(__dirname, '../../cache'));
}

acl.setCachePath(path.resolve(__dirname, '../../cache'));

const test = new Parker();

test.Do('Obtain tokens',
  (p) => {
    process.stdout.write('.');
    acl.getAccount(settings.username, settings.password, settings.aclUrl, function (error, account) {
      if (error) {
        console.error(error);
        p.fail();
        return;
      }
      let service = account.getService('opencart', 'admin', settings.endpoint);
      assert.equal(service.length, 1);
      serviceToken = service[0].UID;
      authToken = account.token;
      request.setEndpoint(settings.endpoint);
      request.setTokens(authToken, serviceToken);
      p.done();
    });
  }
).thenDo('get orders',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('order.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      if (res.body.order === '' || (typeof res.body.order === 'object' && res.body.order.length !== undefined)) {
        console.log('');
        console.log('----------------------------------------------------------------------');
        console.log('Make sure you insert ONE order with the status \'paid\' or \'pending\'');
        console.log('on a webshop NOT IN PRODUCTION for this test to complete');
        process.exit();
      }
      orderId = parseInt(res.body.order.order_id);
      storeId = parseInt(res.body.order.store_id);
      orderStatus = parseInt(res.body.order.order_status_id);
      assert.equal((orderId > 0), true);
      assert.equal((orderStatus > 0), true);
      assert.equal((storeId > -1), true);
      p.done();
    });
  }
);

test.thenDo('Set an order status',
  (p) => {
    process.stdout.write('.');
    var order = {
      order_status: 'complete'
    };
    var pst = request.toXml(order);
    var api = request.getApi('order/status/' + orderId + '.xml');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get order details',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('order/' + orderId + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      orderCompleteStatus = parseInt(res.body.order.order_status_id);
      assert.equal((orderCompleteStatus > 0), true);
      p.done();
    });
  }
).thenDo('Set an order status',
  (p) => {
    process.stdout.write('.');
    var order = {
      order_status: 'pending'
    };
    var pst = request.toXml(order);
    var api = request.getApi('order/status/' + orderId + '.xml');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get order details',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('order/' + orderId + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      orderPendingStatus = parseInt(res.body.order.order_status_id);
      assert.equal((orderPendingStatus > 0), true);
      assert.equal(orderPendingStatus !== orderCompleteStatus, true);
      p.done();
    });
  }
).thenDo('Set an order status',
  (p) => {
    process.stdout.write('.');
    var order = {
      order_status: 'paid'
    };
    var pst = request.toXml(order);
    var api = request.getApi('order/status/' + orderId + '.xml');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get order details',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('order/' + orderId + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      orderPaidStatus = parseInt(res.body.order.order_status_id);
      assert.equal((orderPaidStatus > 0), true);
      assert.equal(orderPaidStatus !== orderCompleteStatus, true);
      p.done();
    });
  }
);

test.thenDo('reset order to initial status',
  (p) => {
    process.stdout.write('.');
    var status;
    if (orderStatus === orderCompleteStatus) {
      status = 'complete';
    } else if (orderStatus === orderPaidStatus) {
      status = 'paid';
    } else if (orderStatus === orderPendingStatus) {
      status = 'pending';
    }
    if (!status) {
      // original status not found
      assert.equal(true, false);
    }
    var order = {
      order_status: status
    };
    var pst = request.toXml(order);
    var api = request.getApi('order/status/' + orderId + '.xml');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get order details',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('order/' + orderId + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      var test = parseInt(res.body.order.order_status_id);
      assert.equal(res.statusCode, 200);
      assert.equal(test, orderStatus);
      p.done();
    });
  }
);

test.whenDone(() => {
  let endTime = new Date().getTime();
  console.log(color.GREEN + 'Succesfull finished in: ' + (endTime - startTime) + ' ms' + color.RESET);
  process.exit();
}).whenFail((e) => {
  process.stdout.write(color.RED);
  console.error(e);
  process.stdout.write(color.RESET);
  process.exit(1);
});
