#!/usr/bin/env node

const assert = require('assert');
const path = require('path');
const fs = require('fs');
const acl = require('../../lib/aclclient.js');
const Parker = require('parker-promise');
const startTime = new Date().getTime();

const request = require('../../lib/ocRequest.js');

var settings;
var authToken;
var serviceToken;
var manufacturer1;

const color = {
  BLACK: '\u001b[30m',
  RED: '\u001b[31m',
  GREEN: '\u001b[32m',
  YELLOW: '\u001b[33m',
  BLUE: '\u001b[34m',
  MAGENTA: '\u001b[35m',
  CYAN: '\u001b[36m',
  WHITE: '\u001b[37m',
  RESET: '\u001b[0m'
};

try {
  let tst = fs.readFileSync(path.resolve(__dirname, '../../settings.json'));
  settings = JSON.parse(tst.toString());
} catch (e) {
  console.error(e);
  console.log('* Unable to open settings.json *');
  console.log('cp settings.json.example settings.json');
  process.exit(1);
}
try {
  fs.statSync(path.resolve(__dirname, '../../cache'));
} catch (e) {
  fs.mkdirSync(path.resolve(__dirname, '../../cache'));
}

acl.setCachePath(path.resolve(__dirname, '../../cache'));

const test = new Parker();

test.Do('Obtain tokens',
  (p) => {
    process.stdout.write('.');
    acl.getAccount(settings.username, settings.password, settings.aclUrl, function (error, account) {
      if (error) {
        console.error(error);
        p.fail();
        return;
      }
      let service = account.getService('opencart', 'admin', settings.endpoint);
      assert.equal(service.length, 1);
      serviceToken = service[0].UID;
      authToken = account.token;
      request.setEndpoint(settings.endpoint);
      request.setTokens(authToken, serviceToken);
      p.done();
    });
  }
).thenDo(' manufacturer index',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('manufacturer.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      if (res.statusCode === 501) {
        console.log('Manufacturers not implemented');
        console.log('Stopping test');
        process.exit(1);
      }
      p.done();
    });
  }
).thenDo('Add a manufacturer',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('manufacturer.xml');
    // This is a complete group (with default values)

    var manufacturer = {
      image_b64: request.readDataFile('image.b64'),
      name: 'test',
      keyword: 'seo_url_voor_test_manufacturer'
    };
    var pst = request.toXml(manufacturer);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw res;
      }
      assert.equal(res.statusCode, 200);
      manufacturer1 = res.body.manufacturer.manufacturer_id;
      assert.equal(res.body.manufacturer.status, 'ok');
      p.done();
    });
  }
).thenDo('Get the manufacturer',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('manufacturer/' + manufacturer1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.manufacturer.name, 'test');
      assert.equal(res.body.manufacturer.image, 'catalog/m/' + manufacturer1 + '/' + manufacturer1 + '_.jpg');
      p.done();
    });
  }
).thenDo('Delete the manufacturer',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('manufacturer/' + manufacturer1 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get the manufacturer',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('manufacturer/' + manufacturer1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
);

test.whenDone(() => {
  let endTime = new Date().getTime();
  console.log(color.GREEN + 'Succesfull finished in: ' + (endTime - startTime) + ' ms' + color.RESET);
  process.exit();
}).whenFail((e) => {
  process.stdout.write(color.RED);
  console.error(e);
  process.stdout.write(color.RESET);
  process.exit(1);
});
