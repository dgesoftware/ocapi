#!/usr/bin/env node

const assert = require('assert');
const path = require('path');
const fs = require('fs');
const acl = require('../../lib/aclclient.js');
const Parker = require('parker-promise');
const startTime = new Date().getTime();

const request = require('../../lib/ocRequest.js');

var settings;
var authToken;
var serviceToken;
var att1;
var group1;
var category1;
var category2;
var catalogusId;
var opencartId;
var opencartId2;
var opencartId3;
var cgroup1;
var cgroup2;

// MAX CAT
var category = {
  parent_id: 0,
  image_b64: request.readDataFile('image.b64'),
  top: 1, // 1 / 0 laat in TOP menu zien
  status: 0, // 1 / 0 enabled (default 1)
  sort_order: 1,
  // Number of columns to use for the bottom 3 categories. Only works for the top parent categories.
  column: 3, // (should this be here?)
  keyword: 'seo_url_voor_test_category',
  // category_store : 0, // or array [0,1] leave blank for default store? (need to figure that one out)
  category_description: {
    nl: {
      name: 'Test Category',
      description: 'for testing puproses',
      meta_description: 'meta description for test puposes',
      meta_keyword: 'meta, keywords, tests'
    }
  }
};

const color = {
  BLACK: '\u001b[30m',
  RED: '\u001b[31m',
  GREEN: '\u001b[32m',
  YELLOW: '\u001b[33m',
  BLUE: '\u001b[34m',
  MAGENTA: '\u001b[35m',
  CYAN: '\u001b[36m',
  WHITE: '\u001b[37m',
  RESET: '\u001b[0m'
};

try {
  let tst = fs.readFileSync(path.resolve(__dirname, '../../settings.json'));
  settings = JSON.parse(tst.toString());
} catch (e) {
  console.error(e);
  console.log('* Unable to open settings.json *');
  console.log('cp settings.json.example settings.json');
  process.exit(1);
}
try {
  fs.statSync(path.resolve(__dirname, '../../cache'));
} catch (e) {
  fs.mkdirSync(path.resolve(__dirname, '../../cache'));
}

acl.setCachePath(path.resolve(__dirname, '../../cache'));

const test = new Parker();

test.Do('Obtain tokens',
  (p) => {
    process.stdout.write('.');
    acl.getAccount(settings.username, settings.password, settings.aclUrl, function (error, account) {
      if (error) {
        console.error(error);
        p.fail();
        return;
      }
      let service = account.getService('opencart', 'admin', settings.endpoint);
      assert.equal(service.length, 1);
      serviceToken = service[0].UID;
      authToken = account.token;
      request.setEndpoint(settings.endpoint);
      request.setTokens(authToken, serviceToken);
      p.done();
    });
  }
).thenDo('Add attribute group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attributegroup.xml');
    // This is a complete group (with default values)
    var group = {
      sort_order: 0,
      attribute_group_description: {
        nl: {
          name: 'attribute_group'
        }
      }
    };
    var pst = request.toXml(group);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw res;
      }
      if (res.statusCode === 501) {
        console.log('Attribute groups not implemented');
        console.log('Stopping test');
        process.exit(1);
      }
      assert.equal(res.statusCode, 200);
      group1 = parseInt(res.body.attributegroup.attribute_group_id);
      assert.equal((group1 > 0), true);
      assert.equal(res.body.attributegroup.status, 'ok');
      p.done();
    });
  }
).thenDo('insert an attribute',
  (p) => {
    process.stdout.write('.');
    // attr_url
    var attr = {
      sort_order: 1,
      attribute_group_id: group1,
      attribute_description: {
        nl: {
          name: 'tester2'
        }
      }
    };
    var api = request.getApi('attribute');
    var pst = request.toXml(attr);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      att1 = parseInt(res.body.attribute_id);
      assert.equal((att1 > 0), true);
      p.done();
    });
  }
).thenDo('Add categories',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category.xml');
    var pst = request.toXml(category, 'category');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      category1 = parseInt(res.body.category.category_id);
      assert.equal((parseInt(category1) > 0), true);
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category.xml');
    category.category_description.nl.name = 'Second cat';
    var pst = request.toXml(category, 'category');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      category2 = parseInt(res.body.category.category_id);
      assert.equal((parseInt(category2) > 0), true);
      p.done();
    });
  }

).thenDo('Data vaildation (all errors)',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product' + '.xml');
    var product = { model: '0987654321789098765432178909876543217890987654321789098765432178909876543217891234' };
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      // Error on model
      assert.equal(res.statusCode, 406);
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product' + '.xml');
    var product = { model: '123' }; // need 4 chars min
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      // error on product description (not a complete product)
      assert.equal(res.statusCode, 406);
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product' + '.xml');
    var product = { price: 'pipo' }; // need 4 chars min
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 406);
      // invalid price
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product' + '.xml');
    var product = {
      catalogus_id: catalogusId,
      unit_id: 1,
      price: 100,
      model: catalogusId,
      product_description: {
        nl: {
          name: '__test KRACHTDOP 1/2&quot; EXTRA LANG TYPE 2 VOOR BOUT MET BUITENZESKANT 23'
        }
      }
    };
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 406);
      // error on catalogus_id
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product' + '.xml');
    var product = {
      catalogus_id: 987654321,
      store_id: 0,
      price: 100,
      status: 0,
      unit_id: 1,
      product_description: {
        nl: {
          name: '__test KRACHTDOP 1/2&quot; EXTRA LANG TYPE 2 VOOR BOUT MET BUITENZESKANT 23',
          unit_description: 'bucket',
          description: 'test'
        }
      },
      product_category: category1,
      product_attribute: [{
        attribute_id: att1,
        product_attribute_description: {
          nl: {text: 'My own attribute'}
        }
      }]
    };
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      // missing model
      assert.equal(res.statusCode, 406);
      p.done();
    });
  }
);
test.thenDo('Add a minimal product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product' + '.xml');
    var product = {
      catalogus_id: 987654321,
      model: 987654321,
      price: 100,
      tax: 'high',
      status: 0,
      unit_id: 1,
      product_description: {
        nl: {
          name: '__test KRACHTDOP 1/2&quot; EXTRA LANG TYPE 2 VOOR BOUT MET BUITENZESKANT 23',
          unit_description: 'bucket',
          description: 'test'
        }
      },
      product_category: category1,
      product_attribute: [{
        attribute_id: att1,
        product_attribute_description: {
          nl: {text: 'My own attribute'}
        }
      }]
    };
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      opencartId = parseInt(res.body.product.product_id);
      assert.equal((opencartId > 0), true);
      p.done();
    });
  }
).thenDo('Get a product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.product.dge_reference.catalogus_id, '987654321');
      assert.equal(res.body.product.dge_reference.unit_id, '1');
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/byref/987654321.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.product.dge_reference.catalogus_id, '987654321');
      assert.equal(res.body.product.dge_reference.unit_id, '1');
      p.done();
    });
  }
);
test.thenDo('Update product categories',
  (p) => {
    process.stdout.write('.');
    var product = {
      // 2 cta
      product_category: [category1, category2]
    };
    var api = request.getApi('product/' + opencartId + '.xml');
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      opencartId = parseInt(res.body.product.product_id);
      assert.equal((opencartId > 0), true);
      p.done();
    });
  }
).thenDo('Check product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.product.product_category.length, 2);
      assert.equal(res.body.product.dge_reference.catalogus_id, '987654321');
      assert.equal(res.body.product.dge_reference.unit_id, '1');
      p.done();
    });
  }
);
test.thenDo('insert customer groups',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup.xml');

    // This is a complete group (with default values)
    var group = {
      approval: 1, // Newly created accounts need approval (default 1)
      company_id_display: 1, // display KVK nr. default 1
      company_id_required: 0, // KVK required, default 0
      tax_id_display: 1, // show VAT nr, default 1
      tax_id_required: 0, // VAT nr required, default 0
      sort_order: 0, // order default 0
      customer_group_description: {
        nl: {
          name: 'test_group_',
          description: 'Ik ben voor test doeleinde'
        }
      }
    };
    var pst = request.toXml(group);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      cgroup1 = parseInt(res.body.customergroup.customer_group_id);
      assert.equal((cgroup1 > 0), true);
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup.xml');

    // This is a complete group (with default values)
    var group = {
      approval: 1, // Newly created accounts need approval (default 1)
      company_id_display: 1, // display KVK nr. default 1
      company_id_required: 0, // KVK required, default 0
      tax_id_display: 1, // show VAT nr, default 1
      tax_id_required: 0, // VAT nr required, default 0
      sort_order: 0, // order default 0
      customer_group_description: {
        nl: {
          name: 'test_group_ 2',
          description: 'Ik ben voor test doeleinde (2)'
        }
      }
    };
    var pst = request.toXml(group);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      cgroup2 = parseInt(res.body.customergroup.customer_group_id);
      assert.equal((cgroup2 > 0), true);
      p.done();
    });
  }
);
test.thenDo('Update product prices',
  (p) => {
    process.stdout.write('.');
    var product = {
      price: 101,
      product_discount: [
        {
          customer_group_id: cgroup1,
          quantity: 10,
          price: 99
        },
        {
          customer_group_id: cgroup2,
          quantity: 20,
          price: 95
        }
      ]
    };
    var api = request.getApi('product/' + opencartId + '.xml');
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      opencartId = parseInt(res.body.product.product_id);
      assert.equal((opencartId > 0), true);
      p.done();
    });
  }
).thenDo('Check product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.product.price, '101.0000');
      assert.equal(res.body.product.product_category.length, 2);
      assert.equal(res.body.product.product_discount.length, 2);
      assert.equal(res.body.product.dge_reference.catalogus_id, '987654321');
      assert.equal(res.body.product.dge_reference.unit_id, '1');
      p.done();
    });
  }
);

test.thenDo('Set and add images',
  (p) => {
    process.stdout.write('.');
    var product = {
      product_image_b64: [request.readDataFile('image.b64'), request.readDataFile('image.b64')],
      image_b64: request.readDataFile('image.b64')
    };
    var api = request.getApi('product/' + opencartId + '.xml');
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      opencartId = parseInt(res.body.product.product_id);
      assert.equal((opencartId > 0), true);
      p.done();
    });
  }

).thenDo('check product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId + '.xml');
    var options = request.options(api, 'get');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.product.price, '101.0000');
      assert.equal(res.body.product.product_image.length, 2);
      assert.equal((res.body.product.image.length) > 0, true);
      assert.equal(res.body.product.product_category.length, 2);
      assert.equal(res.body.product.product_discount.length, 2);
      assert.equal(res.body.product.dge_reference.catalogus_id, '987654321');
      assert.equal(res.body.product.dge_reference.unit_id, '1');
      p.done();
    });
  }
);

test.thenDo('Set dimension and weight',
  (p) => {
    process.stdout.write('.');
    var product = {
      dimensions: {
        width: '1', // 5
        height: '0.5', //
        length: '6',
        length_class: 'cm' // , mm (millimeter), in (Inch)
      },
      weight: {
        weight_class: 'kg', // g (gram), lb (Pound), oz (Ounce)
        weight: '0.02'
      }
    };
    var api = request.getApi('product/' + opencartId + '.xml');
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      opencartId = parseInt(res.body.product.product_id);
      assert.equal((opencartId > 0), true);
      p.done();
    });
  }
).thenDo('check product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId + '.xml');
    var options = request.options(api, 'get');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.product.length, '6.00000000');
      assert.equal(res.body.product.width, '1.00000000');
      assert.equal(res.body.product.height, '0.50000000');
      assert.equal(res.body.product.weight, '0.02000000');
      assert.equal(res.body.product.price, '101.0000');
      assert.equal(res.body.product.product_image.length, 2);
      assert.equal((res.body.product.image.length) > 0, true);
      assert.equal(res.body.product.product_category.length, 2);
      assert.equal(res.body.product.product_discount.length, 2);
      assert.equal(res.body.product.dge_reference.catalogus_id, '987654321');
      assert.equal(res.body.product.dge_reference.unit_id, '1');
      p.done();
    });
  }
);
test.thenDo('Set special',
  (p) => {
    process.stdout.write('.');
    var product = {
      product_special: [
        {
          price: 201.00,
          date_start: '2013-12-01',
          date_end: '0000-00-00'

        },
        {
          customer_group_id: cgroup1,
          priority: 9,
          price: 199.00,
          date_start: '2013-12-01',
          date_end: '0000-00-00'
        }
      ]
    };
    var api = request.getApi('product/' + opencartId + '.xml');
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      opencartId = parseInt(res.body.product.product_id);
      assert.equal((opencartId > 0), true);
      p.done();
    });
  }
).thenDo('check product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId + '.xml');
    var options = request.options(api, 'get');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.product.product_special.length, 2);
      assert.equal(res.body.product.length, '6.00000000');
      assert.equal(res.body.product.width, '1.00000000');
      assert.equal(res.body.product.height, '0.50000000');
      assert.equal(res.body.product.weight, '0.02000000');
      assert.equal(res.body.product.price, '101.0000');
      assert.equal(res.body.product.product_image.length, 2);
      assert.equal((res.body.product.image.length) > 0, true);
      assert.equal(res.body.product.product_category.length, 2);
      assert.equal(res.body.product.product_discount.length, 2);
      assert.equal(res.body.product.dge_reference.catalogus_id, '987654321');
      assert.equal(res.body.product.dge_reference.unit_id, '1');
      p.done();
    });
  }
);
test.thenDo('Add a second product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product' + '.xml');
    var product = {
      catalogus_id: 55555,
      model: 55555,
      price: 100,
      tax: 'low',
      status: 0,
      unit_id: 1,
      product_description: {
        nl: {
          name: 'second product',
          unit_description: 'bucket',
          description: 'test'
        }
      },
      product_category: category2,
      product_attribute: [{
        attribute_id: att1,
        product_attribute_description: {
          nl: {text: 'My own attribute'}
        }
      }]
    };
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      opencartId2 = parseInt(res.body.product.product_id);
      assert.equal((opencartId > 0), true);
      p.done();
    });
  }
).thenDo('Get a product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId2 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.product.dge_reference.catalogus_id, '55555');
      assert.equal(res.body.product.dge_reference.unit_id, '1');
      p.done();
    });
  }
).thenDo('Set the second product as related to the first',
  (p) => {
    process.stdout.write('.');
    var product = {
      product_related: opencartId2
    };
    var api = request.getApi('product/' + opencartId + '.xml');
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      opencartId = parseInt(res.body.product.product_id);
      assert.equal((opencartId > 0), true);
      p.done();
    });
  }
).thenDo('check product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId + '.xml');
    var options = request.options(api, 'get');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.product.product_related, '' + opencartId2);
      assert.equal(res.body.product.product_special.length, 2);
      assert.equal(res.body.product.length, '6.00000000');
      assert.equal(res.body.product.width, '1.00000000');
      assert.equal(res.body.product.height, '0.50000000');
      assert.equal(res.body.product.weight, '0.02000000');
      assert.equal(res.body.product.price, '101.0000');
      assert.equal(res.body.product.product_image.length, 2);
      assert.equal((res.body.product.image.length) > 0, true);
      assert.equal(res.body.product.product_category.length, 2);
      assert.equal(res.body.product.product_discount.length, 2);
      assert.equal(res.body.product.dge_reference.catalogus_id, '987654321');
      assert.equal(res.body.product.dge_reference.unit_id, '1');
      p.done();
    });
  }
);
test.thenDo('update a bunch of fields at once',
  (p) => {
    process.stdout.write('.');
    var product = {
      ean: '654321789abcd',
      location: 'Belgium',
      isbn: 'hey_isbn',
      jan: '1998345',
      sku: 'iamsku',
      mpn: '123',
      upc: '1234'
    };
    var api = request.getApi('product/' + opencartId + '.xml');
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      opencartId = parseInt(res.body.product.product_id);
      assert.equal((opencartId > 0), true);
      p.done();
    });
  }
).thenDo('check product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId + '.xml');
    var options = request.options(api, 'get');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.product.ean, '654321789abcd');
      assert.equal(res.body.product.location, 'Belgium');
      assert.equal(res.body.product.isbn, 'hey_isbn');
      assert.equal(res.body.product.jan, '1998345');
      assert.equal(res.body.product.sku, 'iamsku');
      assert.equal(res.body.product.mpn, '123');
      assert.equal(res.body.product.upc, '1234');
      assert.equal(res.body.product.product_related, '' + opencartId2);
      assert.equal(res.body.product.product_special.length, 2);
      assert.equal(res.body.product.length, '6.00000000');
      assert.equal(res.body.product.width, '1.00000000');
      assert.equal(res.body.product.height, '0.50000000');
      assert.equal(res.body.product.weight, '0.02000000');
      assert.equal(res.body.product.price, '101.0000');
      assert.equal(res.body.product.product_image.length, 2);
      assert.equal((res.body.product.image.length) > 0, true);
      assert.equal(res.body.product.product_category.length, 2);
      assert.equal(res.body.product.product_discount.length, 2);
      assert.equal(res.body.product.dge_reference.catalogus_id, '987654321');
      assert.equal(res.body.product.dge_reference.unit_id, '1');
      p.done();
    });
  }
);

test.thenDo('Copy a product',
  (p) => {
    process.stdout.write('.');
    var product = {
      catalogus_id: 987654321,
      unit_id: 112,
      quantity: 100,
      price: 500,
      product_description: {
        nl: {
          unit_description: 'per strekkende hectoliter'
        }
      }
    };
    var api = request.getApi('product/copy/' + opencartId + '.xml');
    var pst = request.toXml(product);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      opencartId3 = parseInt(res.body.product.product_id);
      assert.equal((opencartId !== opencartId3), true);
      p.done();
    });
  }
).thenDo('check product',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId3 + '.xml');
    var options = request.options(api, 'get');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.product.ean, '654321789abcd');
      assert.equal(res.body.product.location, 'Belgium');
      assert.equal(res.body.product.isbn, 'hey_isbn');
      assert.equal(res.body.product.jan, '1998345');
      assert.equal(res.body.product.sku, 'iamsku');
      assert.equal(res.body.product.mpn, '123');
      assert.equal(res.body.product.upc, '1234');
      assert.equal(res.body.product.product_related, '' + opencartId2);
      assert.equal(res.body.product.length, '6.00000000');
      assert.equal(res.body.product.width, '1.00000000');
      assert.equal(res.body.product.height, '0.50000000');
      assert.equal(res.body.product.weight, '0.02000000');
      assert.equal(res.body.product.price, '500.0000');
      assert.equal(res.body.product.product_image.length, 2);
      assert.equal((res.body.product.image.length) > 0, true);
      assert.equal(res.body.product.dge_reference.catalogus_id, '987654321');
      assert.equal(res.body.product.dge_reference.unit_id, '112');
      p.done();
    });
  }
);

test.thenDo('Delete the products',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId3 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId2 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/delete/' + opencartId + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Check for 404',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId3 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId2 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/' + opencartId + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
);
test.thenDo('Check products with options',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('product/withOptions.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      if (res.statusCode === 404) {
        console.log('No products with options found');
        console.log('Skipping test');
      } else if (res.statusCode === 501) {
        console.log('Get products with options not implemented');
        console.log('Skipping test');
      } else if (res.statusCode === 200) {
        if (res.body.products.length) {
          assert.equal((parseInt(res.body.products[0].product_id) > 0), true);
        } else {
          assert.equal((parseInt(res.body.products.product_id) > 0), true);
        }
      } else {
        assert.equal(true, false);
      }
      p.done();
    });
  }
);

test.thenDo('Delete customer groups',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup1 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup2 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Check for 404s',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup2 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
);

test.thenDo('Delete attribute',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attribute/' + att1 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get an attribute',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attribute/' + att1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
).thenDo('Delete the group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attributegroup/' + group1 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get the group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('attributegroup/' + group1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
);

test.thenDo('Delete categories',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category/' + category1 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.category.category_id, category1 + '');
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category/' + category2 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.category.category_id, category2 + '');
      p.done();
    });
  }
).thenDo('Get the categories',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category/' + category1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  },
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category/' + category2 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
);
test.whenDone(() => {
  let endTime = new Date().getTime();
  console.log(color.GREEN + 'Succesfull finished in: ' + (endTime - startTime) + ' ms' + color.RESET);
  process.exit();
}).whenFail((e) => {
  process.stdout.write(color.RED);
  console.error(e);
  process.stdout.write(color.RESET);
  process.exit(1);
});
