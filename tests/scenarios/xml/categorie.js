#!/usr/bin/env node

const assert = require('assert');
const path = require('path');
const fs = require('fs');
const acl = require('../../lib/aclclient.js');
const Parker = require('parker-promise');
const startTime = new Date().getTime();

const request = require('../../lib/ocRequest.js');

var settings;
var authToken;
var serviceToken;
var category1;
var category2;
var category3;

const color = {
  BLACK: '\u001b[30m',
  RED: '\u001b[31m',
  GREEN: '\u001b[32m',
  YELLOW: '\u001b[33m',
  BLUE: '\u001b[34m',
  MAGENTA: '\u001b[35m',
  CYAN: '\u001b[36m',
  WHITE: '\u001b[37m',
  RESET: '\u001b[0m'
};

// MAX CAT
var category = {
  parent_id: 0,
  image_b64: request.readDataFile('image.b64'),
  top: 1, // 1 / 0 laat in TOP menu zien
  status: 0, // 1 / 0 enabled (default 1)
  sort_order: 1,
  // Number of columns to use for the bottom 3 categories. Only works for the top parent categories.
  column: 3, // (should this be here?)
  keyword: 'seo_url_voor_test_category',
  // category_store : 0, // or array [0,1] leave blank for default store? (need to figure that one out)
  category_description: {
    nl: {
      name: 'Test Category',
      description: 'for testing puproses',
      meta_description: 'meta description for test puposes',
      meta_keyword: 'meta, keywords, tests'
    }
  }
};
// Min cat
var minCat = {
  status: 0, // 1 / 0 enabled (default 1)
  category_description: {
    pipo: 4,
    nl: {
      name: 'Test Category 2'
    }
  }
};
try {
  let tst = fs.readFileSync(path.resolve(__dirname, '../../settings.json'));
  settings = JSON.parse(tst.toString());
} catch (e) {
  console.error(e);
  console.log('* Unable to open settings.json *');
  console.log('cp settings.json.example settings.json');
  process.exit(1);
}
try {
  fs.statSync(path.resolve(__dirname, '../../cache'));
} catch (e) {
  fs.mkdirSync(path.resolve(__dirname, '../../cache'));
}

acl.setCachePath(path.resolve(__dirname, '../../cache'));

const test = new Parker();

test.Do('Obtain tokens',
  (p) => {
    process.stdout.write('.');
    acl.getAccount(settings.username, settings.password, settings.aclUrl, function (error, account) {
      if (error) {
        console.error(error);
        p.fail();
        return;
      }
      let service = account.getService('opencart', 'admin', settings.endpoint);
      assert.equal(service.length, 1);
      serviceToken = service[0].UID;
      authToken = account.token;
      request.setEndpoint(settings.endpoint);
      request.setTokens(authToken, serviceToken);
      console.log(settings.endpoint);
      p.done();
    });
  }
);
test.thenDo('error on name',
  (p) => {
    process.stdout.write('.');
    // error on name
    var api = request.getApi('category.xml');
    category.category_description.nl.name = '';
    var pst = request.toXml(category, 'category');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      // Missing valid name
      assert.equal(res.statusCode, 406);
      // reset object
      category.category_description.nl.name = 'Test category';
      p.done();
    });
  }
);

test.thenDo('error parent_id',
  (p) => {
    process.stdout.write('.');
    // error on parent_id
    var api = request.getApi('category.xml');
    category.parent_id = 'a';
    var pst = request.toXml(category, 'category');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      // invalid parent_id
      assert.equal(res.statusCode, 406);
      // reset object
      category.parent_id = 0;
      p.done();
    });
  }
);
test.thenDo('error sort_order',
  (p) => {
    process.stdout.write('.');
    // error on sort_order
    var api = request.getApi('category.xml');
    category.sort_order = 'a';
    var pst = request.toXml(category, 'category');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 406);
      // invalid sort_order
      // reset object
      category.sort_order = 0;
      p.done();
    });
  }
);
test.thenDo('error filter_id',
  (p) => {
    process.stdout.write('.');
    // error on filter_id
    var api = request.getApi('category.xml');
    category.filter_id = 22.198546;
    var pst = request.toXml(category, 'category');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 406);
      // invalid filter_id
      // reset object
      delete category.filter_id;
      p.done();
    });
  }
);

test.thenDo('Add category 1',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category.xml');
    var pst = request.toXml(category, 'category');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      category1 = parseInt(res.body.category.category_id);
      assert.equal((parseInt(category1) > 0), true);
      p.done();
    });
  }
);
test.thenDo('Add category 2',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category.xml');
    minCat.parent_id = category1;
    var pst = request.toXml(minCat, 'category');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      category2 = parseInt(res.body.category.category_id);
      assert.equal((parseInt(category2) > 0), true);
      p.done();
    });
  }
).thenDo('Add category 3',
  (p) => {
    process.stdout.write('.');
    // category 3 has parent_id category1
    var api = request.getApi('category.xml');
    minCat.category_description.nl.name = 'Category 3';
    minCat.parent_id = category1;
    var pst = request.toXml(minCat, 'category');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      category3 = parseInt(res.body.category.category_id);
      assert.equal((parseInt(category3) > 0), true);
      p.done();
    });
  }
).thenDo('get category 2',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category/' + category2 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(parseInt(res.body.category.category_id), category2);
      assert.equal(parseInt(res.body.category.parent_id), category1);
      p.done();
    });
  }
).thenDo('Change parent_is for catgory 3',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category/' + category3 + '.xml');
    var parentcat = {
      parent_id: category2
    };
    var pst = request.toXml(parentcat, 'category');
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      var categoryTest = parseInt(res.body.category.category_id);
      assert.equal(res.statusCode, 200);
      assert.equal(categoryTest, category3);
      assert.equal(res.body.category.status, 'ok');
      p.done();
    });
  }
).thenDo('get category 3',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category/' + category3 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(parseInt(res.body.category.category_id), category3);
      assert.equal(parseInt(res.body.category.parent_id), category2);
      p.done();
    });
  }
).thenDo('Get categories',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category' + '.xml' + '?start=' + 0 + '&limit=3');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.category.length, 3);
      p.done();
    });
  }
).thenDo('Delete category 3',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category/' + category3 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.category.category_id, category3 + '');
      p.done();
    });
  }
).thenDo('Delete category 2',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category/' + category2 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.category.category_id, category2 + '');
      p.done();
    });
  }
).thenDo('Delete category 1',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category/' + category1 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      assert.equal(res.body.category.category_id, category1 + '');
      p.done();
    });
  }
).thenDo('get category 3',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('category/' + category3 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
);

test.whenDone(() => {
  let endTime = new Date().getTime();
  console.log(color.GREEN + 'Succesfull finished in: ' + (endTime - startTime) + ' ms' + color.RESET);
  process.exit();
}).whenFail((e) => {
  process.stdout.write(color.RED);
  console.error(e);
  process.stdout.write(color.RESET);
  process.exit(1);
});
