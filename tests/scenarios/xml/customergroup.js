#!/usr/bin/env node

const assert = require('assert');
const path = require('path');
const fs = require('fs');
const acl = require('../../lib/aclclient.js');
const Parker = require('parker-promise');
const startTime = new Date().getTime();

const request = require('../../lib/ocRequest.js');

var settings;
var authToken;
var serviceToken;
var cgroup1;

const color = {
  BLACK: '\u001b[30m',
  RED: '\u001b[31m',
  GREEN: '\u001b[32m',
  YELLOW: '\u001b[33m',
  BLUE: '\u001b[34m',
  MAGENTA: '\u001b[35m',
  CYAN: '\u001b[36m',
  WHITE: '\u001b[37m',
  RESET: '\u001b[0m'
};

try {
  let tst = fs.readFileSync(path.resolve(__dirname, '../../settings.json'));
  settings = JSON.parse(tst.toString());
} catch (e) {
  console.error(e);
  console.log('* Unable to open settings.json *');
  console.log('cp settings.json.example settings.json');
  process.exit(1);
}
try {
  fs.statSync(path.resolve(__dirname, '../../cache'));
} catch (e) {
  fs.mkdirSync(path.resolve(__dirname, '../../cache'));
}

acl.setCachePath(path.resolve(__dirname, '../../cache'));

const test = new Parker();

test.Do('Obtain tokens',
  (p) => {
    process.stdout.write('.');
    acl.getAccount(settings.username, settings.password, settings.aclUrl, function (error, account) {
      if (error) {
        console.error(error);
        p.fail();
        return;
      }
      let service = account.getService('opencart', 'admin', settings.endpoint);
      assert.equal(service.length, 1);
      serviceToken = service[0].UID;
      authToken = account.token;
      request.setEndpoint(settings.endpoint);
      request.setTokens(authToken, serviceToken);
      p.done();
    });
  }
).thenDo('upsert a customergroup',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup.xml');

    // This is a complete group (with default values)
    var group = {
      approval: 1, // Newly created accounts need approval (default 1)
      company_id_display: 1, // display KVK nr. default 1
      company_id_required: 0, // KVK required, default 0
      tax_id_display: 1, // show VAT nr, default 1
      tax_id_required: 0, // VAT nr required, default 0
      sort_order: 0, // order default 0
      customer_group_description: {
        nl: {
          name: 'test_group_',
          description: 'Ik ben voor test doeleinde'
        }
      }
    };
    var pst = request.toXml(group);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      cgroup1 = parseInt(res.body.customergroup.customer_group_id);
      assert.equal((cgroup1 > 0), true);
      p.done();
    });
  }
).thenDo('Get the customergroup',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      let test = parseInt(res.body.customergroup.customer_group_id);
      assert.equal(cgroup1, test);
      assert.equal(res.body.customergroup.customer_group_description.nl.name, 'test_group_');
      p.done();
    });
  }
).thenDo('Update the customer group',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup1 + '.xml');

    // This is a complete group (with default values)
    var group = {
      sort_order: 99, // order default 0
      customer_group_description: {
        nl: {
          name: 'test 2'
        }
      }
    };
    var pst = request.toXml(group);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      let test = parseInt(res.body.customergroup.customer_group_id);
      assert.equal(test, cgroup1);
      p.done();
    });
  }
).thenDo('Get the customergroup',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      let test = parseInt(res.body.customergroup.customer_group_id);
      assert.equal(cgroup1, test);
      assert.equal(res.body.customergroup.customer_group_description.nl.name, 'test 2');
      p.done();
    });
  }
).thenDo('Delete the customergroup',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup1 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get the customergroup',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
).thenDo('upsert a customergroup with a long name',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup.xml');

    // This is a complete group (with default values)
    var group = {
      approval: 1, // Newly created accounts need approval (default 1)
      company_id_display: 1, // display KVK nr. default 1
      company_id_required: 0, // KVK required, default 0
      tax_id_display: 1, // show VAT nr, default 1
      tax_id_required: 0, // VAT nr required, default 0
      sort_order: 0, // order default 0
      customer_group_description: {
        nl: {
          name: 'test_group_test_group_test_group_test_group_test_group_test_group_',
          description: 'Ik ben voor test doeleinde'
        }
      }
    };
    var pst = request.toXml(group);
    var options = request.options(api, 'POST', pst.ct, pst.len);
    request.go(options, pst.data, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      cgroup1 = parseInt(res.body.customergroup.customer_group_id);
      assert.equal((cgroup1 > 0), true);
      p.done();
    });
  }
).thenDo('Get the customergroup',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      let test = parseInt(res.body.customergroup.customer_group_id);
      assert.equal(cgroup1, test);
      // the name is shortened
      assert.equal(res.body.customergroup.customer_group_description.nl.name, 'dge_' + cgroup1);
      p.done();
    });
  }
).thenDo('Delete the customergroup',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup1 + '.xml');
    var options = request.options(api, 'DELETE');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 200);
      p.done();
    });
  }
).thenDo('Get the customergroup',
  (p) => {
    process.stdout.write('.');
    var api = request.getApi('customergroup/' + cgroup1 + '.xml');
    var options = request.options(api, 'GET');
    request.go(options, null, function (err, res) {
      if (err) {
        throw err;
      }
      assert.equal(res.statusCode, 404);
      p.done();
    });
  }
);

test.whenDone(() => {
  let endTime = new Date().getTime();
  console.log(color.GREEN + 'Succesfull finished in: ' + (endTime - startTime) + ' ms' + color.RESET);
  process.exit();
}).whenFail((e) => {
  process.stdout.write(color.RED);
  console.error(e);
  process.stdout.write(color.RESET);
  process.exit(1);
});
