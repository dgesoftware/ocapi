const path = require('path')
const fs = require('fs')
const url = require('url')
const http = require('http')
const https = require('https')
const __xml2js__ = require('xml2js')
const Buffer = require('buffer').Buffer

var endpoint
var userToken
var serviceToken

var _xmlToJs = function (xml, cb) {
  var parser = new __xml2js__.Parser({
    normalizeTags: true,
    normalize: true,
    explicitArray: false,
    trim: true,
    strict: true,
    async: false,
    attrkey: '_attr',
    charkey: '_value',
    explicitRoot: false
  })
  return parser.parseString(xml, function (err, result) {
    cb(err, result)
  })
}

var _jsToXml = function (ob, root) {
  if (!root) {
    root = 'response'
  }
  var js2xmlparser = require('js2xmlparser')
  return js2xmlparser.parse(root, ob, {
    attributeString: '_attr',
    valueString: '_value'
  })
}

var _createRequest = function (options, data, cb) {
  var response = {}
  response.body = ''
  var _http
  if (options.protocol === 'https:') {
    _http = https
  } else {
    _http = http
  }

  var req = _http.request(options, function createHttpRequest (res) {
    response.statusCode = res.statusCode
    response.headers = res.headers
    res.setEncoding('utf8')
    res.on('data', function (chunk) {
      response.body += chunk
    })

    res.on('end', function (x) {
      var accept = response.headers['content-type'] || ''
      var ct = /application\/json/.test(accept) ? 'json' : /application\/xml/.test(accept) ? 'xml' : '.json'

      // @TODO omzetten van de BODY
      if (ct === 'json') {
        try {
          response.body = JSON.parse(response.body)
        } catch (e) {
          response.body = false
        }
        cb(null, response)
      } else if (ct === 'xml') {
        _xmlToJs(response.body, function (err, res) {
          if (err) {
            cb(err)
            return
          }
          response.body = res
          cb(null, response)
        })
      } else {
        cb(null, response)
      }
    })
  })
  req.on('error', function catchError (e) {
    cb(e)
  })

  req.setTimeout(1000 * 10, function () {
    // req.abort()
    cb(new Error('Request timeout'))
  })
  if (data) {
    req.write(data)
  }
  req.end()
}

var _getOptions = function (endpoint, method, contenttype, length) {
  var pathInfo = url.parse(endpoint)
  if (!pathInfo.port && pathInfo.protocol === 'http:') {
    pathInfo.port = 80
  } else if (!pathInfo.port && pathInfo.protocol === 'https:') {
    pathInfo.port = 443
  }

  // Jasmine props are set in the JAKE file
  var header = {
    'x-auth-token': userToken,
    'x-service-token': serviceToken
  }

  if (contenttype && length) {
    header['Content-Type'] = contenttype
    header['Content-Length'] = length
  }

  return {
    protocol: pathInfo.protocol,
    hostname: pathInfo.hostname,
    method: method,
    port: pathInfo.port,
    path: pathInfo.path,
    headers: header
  }
}

// Read file sync
var _readFile = function (fl) {
  return fs.readFileSync(path.resolve(__dirname, './../data/', fl)).toString()
}

var publicApi = {
  toJson: function (ob) {
    var data = JSON.stringify(ob)
    var len = Buffer.byteLength(data)
    return {
      data: data,
      len: len,
      ct: 'application/json'
    }
  },

  toXml: function (ob, entity) {
    if (!entity) {
      entity = 'root'
    }

    var data = _jsToXml(ob, entity)
    var len = Buffer.byteLength(data)
    return {
      data: data,
      len: len,
      ct: 'application/xml'
    }
  },

  xmlToJs: function (str, cb) {
    _xmlToJs(str, function (err, res) {
      cb(err, res)
    })
  },
  // ### getApi(namespace)
  // @TODO: depending on dev/production platform
  getApi: function (ns) {
    return endpoint + ns
    // }
  },

  go: _createRequest,

  options: _getOptions,

  readDataFile: function (path) {
    return _readFile(path)
  },
  type: function (fn) {
    if (typeof fn === 'undefined') return 'undefined'
    return ({}).toString.call(fn).match(/\s([a-z|A-Z]+)/)[1]
  },
  setTokens: function (token, service) {
    userToken = token
    serviceToken = service
  },
  setEndpoint: function (path) {
    endpoint = path
  }
}
module.exports = publicApi
