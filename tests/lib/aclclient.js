// NODEJS acl client
// -------------------------- EXAMPLE ------------------------------------------
// var acl = require('./utils/aclClient.js')

// acl.setCachePath(process.cwd()+'/cache/')

// var res = acl.getAccount('opencart','opencart','https://jd.windev.nl:9010/v1/',function (err, account) {
//   if (err) {
//     console.log(err)
//   } else {
//     console.log(account)
//     account.logout(function () {
//       console.log('LOGGED OUT')
//     })
//     test some general functions
//   }
// })
//
var fs = require('fs')
var url = require('url')
var http = require('http')
var https = require('https')
var crypto = require('crypto')

var secretKey = 'What !*834567a'
var cachePath = '/tmp' // Path to cache dir

/**
 * aclAccount constructor
 *
 * @param {string} file path to cache file
 * @param {string} url Url to ACL endpoint
 * @param {object} data Account data
 * @returns {object} this
 */
var AclAccount = function (file, url, data) {
  var self = this
  this.acl = url
  this.name = data.token.tenant.name
  this.uid = data.token.tenant.id
  this.token = data.token.auth_key
  this.services = data.services
  this.reference_id = data.token.tenant.reference_id
  var cacheFile = file

  /**
   * logout
   *
   * @param {function} cb Callback err, result
   * @returns {object} self
   */
  this.logout = function (cb) {
    fs.unlinkSync(cacheFile)
    this.services = []
    if (!self.token) {
      cb(null, true)
    } else {
      privateApi.logout(self, function (err, res) {
        cb(err, res)
      })
    }
    return self
  }
  /**
   * getService
   * All params are optional
   *
   * @param {string} service_name Service name to find
   * @param {string} role user | author | admin
   * @param {string} end_point url to service endpoint
   * @param {string} uid Service UID
   * @returns {object} Service data
   */
  this.getService = function (serviceName, role, endPoint, uid) {
    var res = []
    var len
    if (this.services) {
      len = this.services.length
    } else {
      len = 0
    }
    var i = 0
    for (; i < len; i++) {
      if (
        (!serviceName || this.services[i].service === serviceName) &&
        (!role || this.services[i].role === role) &&
        (!endPoint || this.services[i].endpoint === endPoint) &&
        (!uid || this.services[i].UID === uid)

      ) {
        res.push(this.services[i])
      }
    }
    return res
  }
}

const garbageCollect = function () {
  var maxAge = 1000 * 60 * 60 * 25 // 25 HOURS
  fs.readdir(cachePath, function (err, files) {
    if (err) {
      // nothing
    }
    files.forEach(function (file) {
      fs.stat(cachePath + '/' + file, function (err, stat) {
        var endTime, now
        if (err) {
          return
        }
        now = new Date().getTime()
        endTime = new Date(stat.ctime).getTime() + maxAge
        if (now > endTime) {
          if (/(\.tkn|\.ndc)$/i.test(file)) {
            var fp = cachePath + '/' + file
            try {
              fs.unlinkSync(fp)
            } catch (e) {
              // There is no need for this log, disturbing it is
              // console.error(e) //eslint-disable-line
            }
          }
        }
      })
    })
  })
}

setInterval(function () {
  garbageCollect()
}, 1000 * 60 * 60 * 25)

/**
 * toJson
 *
 * @param {object} ob Object to transform to json
 * @returns {object} data: jsonString, len:real Content-Length, ct: Content-type
 */
var toJson = function (ob) {
  var data = JSON.stringify(ob)
  var len = Buffer.byteLength(data)
  return {
    data: data,
    len: len,
    ct: 'application/json'
  }
}

var _getOptions = function (endpoint, method, contenttype, length) {
  var pathInfo = url.parse(endpoint)
  if (!pathInfo.port && pathInfo.protocol === 'http:') {
    pathInfo.port = 80
  } else if (!pathInfo.port && pathInfo.protocol === 'https:') {
    pathInfo.port = 443
  }

  var header

  if (contenttype && length) {
    header = {
      'Content-Type': contenttype,
      'Content-Length': length
    }
  } else {
    header = {}
  }

  header['User-Agent'] = 'AC-client'

  return {
    protocol: pathInfo.protocol,
    hostname: pathInfo.hostname,
    method: method,
    port: pathInfo.port,
    path: pathInfo.path,
    headers: header
  }
}

var _createRequest = function (options, data, cb) {
  // Need to branch for HTTPS sometime

  var response = {}
  response.body = ''
  var _http
  if (options.protocol === 'https:') {
    _http = https
  } else {
    _http = http
  }
  var req = _http.request(options, function createHttpRequesti (res) {
    response.statusCode = res.statusCode
    response.headers = res.headers
    res.setEncoding('utf8')
    res.on('data', function (chunk) {
      response.body += chunk
    })
    res.on('end', function () {
      // WE SHOULD HAVE JSON OR XML
      // PARSE IT RIGHT AWAY
      var accept = response.headers['content-type'] || ''
      if (/application\/json/.test(accept)) {
        try {
          response.body = JSON.parse(response.body)
        } catch (e) {
          cb(e.toString())
          return
        }
        cb(null, response)
      } else {
        cb(new Error('No JSON data'), null)
      }
    })
  })
  req.setTimeout(1000 * 5, function () {
    req.abort()
    // cb(new Error('Request timeout'))
  })
  req.on('error', function catchError (e) {
    cb(e, null)
  })

  if (data) {
    req.write(data)
  }
  req.end()
}

var canWrite = function (owner, inGroup, mode) {
  var os = require('os')
  if (os.type() === 'Windows_NT') {
    return true
  }
  owner = process.getuid() === owner
  inGroup = process.getgid() === inGroup
  return (owner && (mode & 200) > 0) || // User is owner and owner can write.
    (inGroup && (mode & 20) > 0) || // User is in group and group can write.
    (mode & 2) > 0 // Anyone can write.
}

var cacheFilenamename = function (str, ext) {
  if (!ext) {
    ext = 'ndc'
  }
  var file = privateApi.encrypt(str, true)
  file = cachePath + '/' + file + '.' + ext
  return file
}

var privateApi = {
  /**
   * getToken
   *
   * @param {string} usr username
   * @param {string} pwd Password
   * @param {string} aclUrl Url to ACL endpoint
   * @param {function} cb callback, err, result
   * @returns {object} self
   */
  getToken: function (usr, pwd, aclUrl, cb) {
    var file = cacheFilenamename(usr + pwd + aclUrl)
    this.readCache(file, function (err, res) {
      if (err) {
        // onmit error
      }
      if (res) {
        cb(null, new AclAccount(file, aclUrl, res))
      } else {
        privateApi.login(usr, pwd, aclUrl, function (err, res) {
          if (err) {
            cb(err, null)
          } else {
            cb(null, new AclAccount(file, aclUrl, res))
          }
        })
      }
    })
    return this
  },

  /**
   * getTokenInfo
   * Get userInfo based on token
   *
   * @param {string} token user token
   * @param {string} aclUrl Url to ACL endpoint
   * @param {function} cb Callback, error, result
   * @returns {object} self
   */
  getTokenInfo: function (token, aclUrl, cb) {
    var file = cacheFilenamename(token + aclUrl, 'tkn')
    this.readCache(file, function (err, res) {
      if (err) {
        // onmit error
      }
      if (res) {
        cb(null, new AclAccount(file, aclUrl, res))
      } else {
        privateApi.getUser(token, aclUrl, function (err, res) {
          if (err) {
            cb(err, null)
          } else {
            cb(null, new AclAccount(file, aclUrl, res))
          }
        })
      }
    })
    return this
  },
  /**
   * getServiceInfo
   * Get userInfo based on token
   *
   * @param {string} token user token
   * @param {string} service uid
   * @param {string} aclUrl Url to ACL endpoint
   * @param {function} cb Callback, error, result
   * @returns {object} self
   */
  getServiceInfo: function (token, serviceUid, aclUrl, cb) {
    var file = cacheFilenamename(token + serviceUid + aclUrl, 'srv')
    this.readCache(file, function (err, res) {
      if (err) {
        // onmit error
      }
      if (res) {
        cb(null, res)
      } else {
        privateApi.getService(token, serviceUid, aclUrl, function (err, res) {
          if (err) {
            cb(err, null)
          } else {
            cb(null, res)
          }
        })
      }
    })
    return this
  },

  /**
   * readCache
   *
   * @param {string} file Path to File
   * @param {function} cb Callback, error, result
   * @returns {object} self
   */
  readCache: function (file, cb) {
    var self = this
    fs.stat(file, function (err, stats) {
      if (err) {
        cb(new Error('not found'), null)
      } else {
        var d = new Date()
        // Check AGE
        var fdate = new Date(stats.ctime)
        var diff = Math.ceil((d - fdate) / 1000 / 60 / 60) // time is in msec
        if (diff > 12) { // Expire in 12 hours (so long running proccesses can take 12 hours)
          // remove cache
          try {
            fs.unlinkSync(file)
          } catch (e) {
            // console.log('Error removing expired file:'+file) //eslint-disable-line
          }
          cb(new Error('Cache expired'), null)
        } else {
          fs.readFile(file, function (err, data) {
            if (err) {
              cb(err)
            } else {
              var str = false
              try {
                str = JSON.parse(self.decrypt(data.toString()))
              } catch (e) {
                str = false
              }
              if (str) {
                cb(null, str)
              } else {
                cb(new Error('Unable to decode'), null)
              }
            }
          })
        }
      }
    })
    return this
  },
  /**
   * cache
   *
   * @param {string} file Full path to file
   * @param {object} data Data to store/cache
   * @param {function} cb callback (errot, result)
   * @returns {object} this
   */
  cache: function (file, data, cb) {
    var storedata = this.encrypt(JSON.stringify(data))
    fs.open(file, 'w', function (err, fd) {
      if (err) {
        cb(err)
        return this
      } else {
        fs.write(fd, storedata, undefined, undefined, function (err) {
          fs.close(fd, function () {})
          if (err) {
            cb(err)
          } else {
            cb(null, data)
          }
        })
      }
    })
    return this
  },
  /**
   * login
   *
   * @param {string} usr username
   * @param {string} pwd password
   * @param {string} aclUrl url to Acl endpoint
   * @param {function} cb callback(error,result)
   * @returns {object} this
   */
  login: function (usr, pwd, aclUrl, cb) {
    var tpl = {
      'auth': {
        'methods': 'password',
        'password': {
          'user': {
            'username': '' + usr,
            'password': '' + pwd
          }
        }
      }
    }
    var data = toJson(tpl)
    var file = cacheFilenamename(usr + pwd + aclUrl)
    var reqOptions = _getOptions(aclUrl + 'tokens.json', 'POST', data.ct, data.len)
    _createRequest(reqOptions, data.data, function (err, res) {
      if (err || res.statusCode !== 200 || !res.body) {
        if (err) {
          cb(err)
        } else {
          cb(new Error('Check your credentials'), null)
        }
      } else {
        privateApi.cache(file, res.body, function (err, res) {
          cb(err, res)
        })
      }
    })
    return this
  },
  /**
   * getUser
   *
   * @param {string} token user token
   * @param {string} aclUrl Url to Acl endpoint
   * @param {function} cb callback(error, result)
   * @returns {object} this
   */
  getUser: function (token, aclUrl, cb) {
    var file = cacheFilenamename(token + aclUrl, 'tkn')
    var reqOptions = _getOptions(aclUrl + 'tokens.json', 'GET')
    reqOptions.headers['x-auth-token'] = token
    reqOptions.headers['x-subject-token'] = token
    _createRequest(reqOptions, null, function (err, res) {
      if (err || res.statusCode !== 200 || !res.body) {
        if (err) {
          cb(err)
        } else {
          cb(new Error('Check your token'), null)
        }
      } else {
        privateApi.cache(file, res.body, function (err, res) {
          cb(err, res)
        })
      }
    })
    return this
  },
  /**
   * getService
   *
   * @param {string} token user token
   * @param {string} service uid
   * @param {string} aclUrl Url to Acl endpoint
   * @param {function} cb callback(error, result)
   * @returns {object} this
   */
  getService: function (token, serviceUid, aclUrl, cb) {
    var file = cacheFilenamename(token + serviceUid + aclUrl, 'srv')
    var reqOptions = _getOptions(aclUrl + 'endpoint/' + serviceUid, 'GET')
    reqOptions.headers['x-auth-token'] = token
    reqOptions.headers['x-subject-token'] = token
    _createRequest(reqOptions, null, function (err, res) {
      if (err || res.statusCode !== 200 || !res.body) {
        if (err) {
          cb(err)
        } else {
          cb(new Error('Check your tokens'), null)
        }
      } else {
        privateApi.cache(file, res.body, function (err, res) {
          cb(err, res)
        })
      }
    })
    return this
  },

  /**
   * logout
   *
   * @param {object} aclAccount User/Account object
   * @param {function} cb callback(error, result)
   * @returns {object} this
   */
  logout: function (aclAccount, cb) {
    var options = _getOptions(aclAccount.acl + 'tokens', 'DELETE')
    options.headers['x-auth-token'] = aclAccount.token
    options.headers['x-subject-token'] = aclAccount.token
    _createRequest(options, false, function () {
      cb(null, true)
    })
    return this
  },
  /**
   * encrypt
   * short, is used as filename
   * @param {string} str String to encrypt
   * @param {boolean} short to hex if true
   * @returns {string} encrypted string
   */
  encrypt: function (str, short) {
    // SHORT IS HEX BASED (and is used as filename)
    var cipher = crypto.createCipher('aes-256-cbc', secretKey)
    var uc
    if (short) {
      uc = cipher.update(str, 'utf8', 'hex')
      return cipher.final('hex')
    } else {
      uc = cipher.update(str, 'utf8', 'base64')
      return uc + cipher.final('base64')
    }
  },
  /**
   * decrypt
   *
   * @param {string} str Encrypted string
   * @returns {string} Decrypted string
   */
  decrypt: function (str) {
    var decipher = crypto.createDecipher('aes-256-cbc', secretKey)
    var ec = decipher.update(str, 'base64', 'utf8')
    return ec + decipher.final('utf8')
  }
}

var publicApi = {
  /**
   * getAccount
   *
   * @param {string} usr username
   * @param {string} pwd password
   * @param {string} aclUrl Url to Acl endpoint
   * @param {function} cb callback(error,result)
   * @returns {object} this
   */
  getAccount: function (usr, pwd, aclUrl, cb) {
    privateApi.getToken(usr, pwd, aclUrl, cb)
    return this
  },
  /**
   * setCachePath
   *
   * @param {string} str Path to cache folder
   * @returns {object} this
   */
  setCachePath: function (str) {
    // Does the path exsist and is it writable?
    // Throw when
    if (/(\/)$/.test(str)) {
      throw new Error('Cache path with trailing slash')
    }
    var stat = fs.statSync(str)
    var writable = canWrite(stat.uid, stat.gid, stat.mode)
    if (!writable) {
      throw new Error('Cache dir not writeable')
    }
    cachePath = str
    garbageCollect(cachePath)
    return this
  },
  /**
   * getTokenInfo
   *
   * @param {string} token user token
   * @param {string} aclUrl url to Acl endpoint
   * @param {function} cb callback(error, result)
   * @returns {object} this
   */
  getTokenInfo: function (token, aclUrl, cb) {
    privateApi.getTokenInfo(token, aclUrl, cb)
    return this
  },
  /**
   * getTokenInfo
   *
   * @param {string} token user token
   * @param {string} aclUrl url to Acl endpoint
   * @param {function} cb callback(error, result)
   * @returns {object} this
   */
  getServiceInfo: function (token, serviceUid, aclUrl, cb) {
    privateApi.getServiceInfo(token, serviceUid, aclUrl, cb)
    return this
  }

}

module.exports = publicApi
