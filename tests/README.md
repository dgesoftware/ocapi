# Opcapi / Magapi test

Install nodejs (http://www.nodejs.org)

Copy 'settings.example.json' to settings.json. The credentials can be obtained at DGE, or bypass the authorisation procedure.
```
{
  "aclUrl": "https://[acl]/v1/",
  "username":"yourname",
  "password":"yourpass",
  "endpoint":"http://[endpoint/api/]"
}
```

Install the node modules/dependencies:

```
npm install
```

Run the XML test:

```
npm run xml:all
```

or run the json test

```
npm run json:all
```

Solitair tests can be runned from the command line
e.g.:

```
scenarios/xml/product.js
```
